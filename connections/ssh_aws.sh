#!/bin/bash

CERT_REPO=/usr/sense/creds/sshkeys/$USER
PK_FILE=${CERT_REPO}/monitor_tunnel_user_20170726.pem

if [[ ! -e ${CERT_REPO} ]]; then
    echo "Make sure you have checked out the certs repo at ${CERT_REPO}"
    exit 1
fi

if [[ ! -e ${PK_FILE} ]]; then
    echo "Can't find ssh key ${PK_FILE}"
    echo "You may need to run blackbox_decrypt_all_files"
    exit 1
fi

ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l ubuntu -i ${PK_FILE} $@
