#!/bin/bash


KEY_PATH=/usr/sense/creds/sshkeys/$USER/monitor_ssh.key
chmod 600 ${KEY_PATH}
if [ -z "$SSH_AGENT_PID" ]; then
	eval `ssh-agent`
	agent_pid=$SSH_AGENT_PID
fi;
ssh-add ${KEY_PATH}

args=
if [ $# -eq 0 ]; then
	echo "No serial specified, defaulting to old tunneling method"
	args="ssh -t -C root@localhost -p 5000 -o UserKnownHostsFile=/dev/null -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no $args"
	/data/shared-support/code/support-tools/tools/connections/ssh_aws.sh st.sense.com -i ${KEY_PATH} -C -A -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "$args"
else
	args=\"$@\"
	/data/shared-support/code/support-tools/tools/connections/ssh_aws.sh st.sense.com -i ${KEY_PATH} -C -A -t -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "/opt/sense/st/st_client_connect.sh $args"
fi;

if [ ! -z "$agent_pid" ]; then
	kill $agent_pid
fi;
