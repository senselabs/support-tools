import argparse
from subprocess import os
import requests
from time import sleep
from getpass import getpass
import sys
from pprint import pprint
from senseSupportTools.Support import Auth

help_text = """

        If you see this message after running the script to make a change, ensure you are using correct parameters.

        Format: channelMapChange.py $house $parameter $value
        $house:
            House number | Ex: h1111
        $parameters:
            swap_mains -- This reflects "LEFT_RIGHT_MAINS_SWAPPED" in the channel map and takes two arguments | Ex: 0,0
                This will act as if flipping polarities for both mains CTs while swapping the channels.

            swap_solar -- This reflects "LEFT_RIGHT_SOLAR_SWAPPED" in the channel map and takes two arguments | Ex: 0,0
                This will act as if flipping polarities for both solar CTs while swapping the channels.

            main_polarity -- This reflects "MAIN_CT_POLARITY_BACKWARDS" in the channel map and takes one argument | Ex: 0,0
                This will change the individual polarity of a main CT {1,0} First value will flip CH 0, second value will flip CH 1

            num_channel -- This reflects "NUM_MAINS_CHANNELS" in the channel map and takes one argument | Ex: 0
                This will change the amount of CTs active. -- You should RARELY need this!

            solar_polarity -- This reflects "SOLAR_CT_POLARITY_BACKWARDS" in the channel map and takes two arguments | Ex: 0,0
                This will change the individual polarity of a solar CT {1,0} First value will flip CH 0, second value will flip CH 1

            using_solar -- This reflects "USING_SOLAR" in channel map; takes 1 value | Ex: 0
                This will enable solar on the monitor if set to 1 | ex: 1

            enable_breakers -- This reflects "SOLAR_CONNECTED_TO_BREAKERS" in the channel map; takes 1 value | Ex: 0
                This will allow you to enable{1} or disable{0} if solar CTs are downstream from Sense

            frame_rate -- This reflects "FRAME_RATE" in the channel map and takes one argument | Ex: 60
                This changes the frequency value. -- You should RARELY need this!
                60 = US
                50 = Most EU countries

        $value:
            0 = false
            1 = true
"""


def restart_wattscoop_request_hello(house, auth):
    os.system('clear')
    script = 'python ~/code/pytools/sense_api_client/sense_api_client/utils/monitor_config.py '
    call = (script + house + ' --reload')
    sleep(1)
    os.system(call)
    sleep(1)
    headers = {"Authorization": 'bearer {}'.format(auth)}
    url = 'https://api.sense.com/api/v1/admin/monitors/{}/request_hello'.format(
        house)
    hello = requests.post(url, headers=headers).json()
    print('Requested Hello')


def check_online_status(house, auth):
    url = 'https://api.sense.com/api/v1/admin/monitors/{}/status'.format(house)
    headers = {'Authorization': 'bearer {}'.format(auth)}
    try:
        get_online_status = requests.get(url, headers=headers).json()

        online_status = get_online_status['monitor_status']['connected']
    except KeyError as e:
        print("Error: {}".format(e))
        online_status = False
        sys.exit()

    return online_status


def parse_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument('house', type=str)

    group = parser.add_argument_group()

    group.add_argument("update", nargs='?', type=str, choices=[
                       "swap_mains", "swap_solar", "main_polarity", "num_channels", "solar_polarity", "swap_solar_mains", "frame_rate", "using_solar", "enable_breakers"])
    group.add_argument("value", nargs='?', type=str)

    args = parser.parse_args()

    return args


args = parse_opts()
house = str(args.house)
channel_param = ''
value = ''
final_value = ''
two_option_value = ["0,0", "0,1", "1,0", "1,1"]
one_option_value = ["0", '1']
num_channels_value = ["1", "2"]
freq_value = ["50", "60"]
one_arg_help = "This requires one arguments. Example: 0"
two_arg_help = "This requires two arguments. Example: 0,0"
callScript = "python2 ~/code/pytools/sense_api_client/sense_api_client/utils/monitor_config.py " + \
    house + " --set channel_map "


def swap_mains(house, value):
    final_param = "LEFT_RIGHT_MAINS_SWAPPED"
    if value in one_option_value:
        os.system(callScript + final_param + ' ' + value)

    else:
        print(one_arg_help)


def swap_solar(house, value):
    final_param = "LEFT_RIGHT_SOLAR_SWAPPED"
    if value in one_option_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print(one_arg_help)


def main_polarity(house, value):
    final_param = "MAIN_CT_POLARITY_BACKWARDS"
    if value in two_option_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print(two_arg_help)


def solar_polarity(house, value):
    final_param = "SOLAR_CT_POLARITY_BACKWARDS"
    if value in two_option_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print(two_arg_help)


def num_channels(house, value):
    final_param = "NUM_MAINS_CHANNEL"
    if value in num_channels_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print("Please enter the number of main CTs")


def using_solar(house, value):
    final_param = "USING_SOLAR"
    if value in one_option_value:
        os.system(callScript + final_param + ' ' + value)
        sleep(1)
    else:
        print(one_arg_help)


def enable_breakers(house, value):
    final_param = "SOLAR_CONNECTED_TO_BREAKERS"
    if value in one_option_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print(one_arg_help)


def set_frequency(house, value):
    final_param = "FRAME_RATE"
    if value in freq_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print("Frequency options are 50 or 60 (hz)")


def swap_mains_solar(house, value):
    final_param = "SOLAR_MAINS_SWAPPED"
    if value in one_option_value:
        os.system(callScript + final_param + ' ' + value)
    else:
        print(one_arg_help)


def check_change(channel_param, channel_options, current_channel_map_script, house, value, auth):
    if channel_param == channel_options[0]:
        swap_mains(args.house, value)
    elif channel_param == channel_options[1]:
        swap_solar(house, value)
    elif channel_param == channel_options[2]:
        main_polarity(house, value)
    elif channel_param == channel_options[3]:
        num_channels(house, value)
    elif channel_param == channel_options[4]:
        solar_polarity(house, value)
    elif channel_param == channel_options[5]:
        swap_mains_solar(house, value)
    elif channel_param == channel_options[6]:
        set_frequency(house, value)
    elif channel_param == channel_options[7]:
        using_solar(house, value)
        restart_wattscoop_request_hello(house, auth)
    elif channel_param == channel_options[8]:
        enable_breakers(house, value)
        restart_wattscoop_request_hello(house, auth)

    else:
        os.system(current_channel_map_script)


def main():
    auth = get_token()
    house = str(args.house)
    if house[0] == 'h':
        house = house[1:]
    channel_param = str(args.update)
    value = str(args.value)
    online = check_online_status(house, auth)
    if online == False:
        print("Monitor is offline.")
        sys.exit()
    channel_options = ["swap_mains", "swap_solar", "main_polarity", "num_channels",
                       "solar_polarity", "swap_solar_mains", "frame_rate", "using_solar", "enable_breakers"]
    current_channel_map_script = "python ~/code/pytools/sense_api_client/sense_api_client/utils/monitor_config.py " + house
    check_change(channel_param, channel_options,
                 current_channel_map_script, house, value, auth)


main()
