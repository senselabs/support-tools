from Sense.Api import Monitor, Auth
import argparse


def parse_opts():
    args = argparse.ArgumentParser()
    args.add_argument("house")
    parser = args.parse_args()
    return parser


def main():
    house = parse_opts().house
    monitor = Monitor(house, Auth().token)
    push = monitor.push_signal_check()
    if push == 200:
        print("Monitor: h{} was pushed through signal check successfully.".format(
            monitor.monitor_id))
    else:
        print("Failed to push monitor: h{} through signal check...\nError Code: {}".format(
            monitor.monitor_id, push))


main()
