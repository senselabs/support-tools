from setuptools import setup


setup(
    name="SupportTools",
    version="0.95",
    author="alex",
    packages=["SupportTools"],
    install_requires=["requests",
                      "gspread"]
)
