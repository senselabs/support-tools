__author__ = 'zach'

import datetime
from zenpy import Zenpy
from datadog import initialize, api
import re
import logging

agents_to_remove = [362778390468, 361896881047, 25731830467, 19775775467, 23652312548, 7929649708, 4431754068, 1218119317, 1183238827]
signal_tags      = ['signal:channelcheck',
                    'signal:ctopen',
                    'signal:freq',
                    'signal:missedwattage',
                    'signal:phase',
                    'signal:solar',
                    'signal:solarcalibration',
                    'signal:solarinmains',
                    'signal:solarmismatch',
                    'signal:voltage',
                    'signal:wattage']

APP_NAME = 'zendesk'

creds = {
    'email' : 'shakwan@sense.com',
    'token' : '2fJhRZltjqQPmviZHthQ7YkOzLee8q0E05LFeUyr',
    'subdomain': 'senseapp'
}

options = {
    'api_key': 'ee1ac599620f1efb82c8bb20ac9ebd5d',
    'app_key': 'b5bc836e96a2fd18e2bc6eeadc006bb62c9a87b1'
}

initialize(**options)
zenpy_client = Zenpy(**creds)
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

now = datetime.datetime.now()

# count business hours open
def countBusinessHours(date):
    hoursOpen = 0
    oneHour   = datetime.timedelta(hours=1)

    while date < now:
        date = date + oneHour
        if date.hour >= 9 and date.hour < 17 and date.weekday() >= 0 and date.weekday() < 5:
            hoursOpen = hoursOpen + 1

    return(hoursOpen)

# NEW AND OPEN TICKETS (not using rn)
def getOldestOpenOrNew():
    openAndNewTickets = zenpy_client.search(type='ticket', status_less_than='pending')

    oldestDate = now
    for ticket in openAndNewTickets:
        tua = ticket.updated_at
        ticketUpdated = datetime.datetime(int(tua[0:4]), int(tua[5:7]), int(tua[8:10]), int(tua[11:13]), int(tua[14:16]), int(tua[17:19]))
        if (ticketUpdated < oldestDate) and (ticket.assignee_id not in agents_to_remove):
            oldestDate = ticketUpdated
            oldestID   = ticket.id

    # adjust for timezone
    oldestDate = oldestDate - datetime.timedelta(hours=4)
    hoursOpen  = countBusinessHours(oldestDate)

    print("NEW AND OPEN")
    print("Oldest Ticket:", oldestID)
    print("Last Update:  ", oldestDate)
    print("Open for:     ", hoursOpen, "business hours")

    # send to datadog
    logger.info("{}.oldestTicketID: {}".format(APP_NAME, oldestID))
    api.Metric.send(metric="{}.oldestTicketID".format(APP_NAME), points=(oldestID))
    logger.info("{}.hoursOpen: {}".format(APP_NAME, hoursOpen))
    api.Metric.send(metric="{}.hoursOpen".format(APP_NAME), points=(hoursOpen))

# get oldest open or new ticket w/o any replies or comments at all. not including signal support
def getOldestOpenNoResponse():
    openAndNewTickets = zenpy_client.search(type='ticket', status_less_than='pending')

    oldestDate = now
    oldestID   = 1
    for ticket in openAndNewTickets:
        # Count number of ticket comments
        nComments = 0
        for audit in zenpy_client.tickets.audits(ticket.id):
            for event in audit.events:
                if event["type"] == 'Comment':
                    nComments = nComments + 1

        tua = ticket.updated_at
        ticketUpdated = datetime.datetime(int(tua[0:4]), int(tua[5:7]), int(tua[8:10]), int(tua[11:13]), int(tua[14:16]), int(tua[17:19]))
        # if the only comment is the initial one and its older then the current oldest and it's a support ticket, then this ticket is now oldest
        if nComments == 1 and (ticketUpdated < oldestDate) and (ticket.assignee_id not in agents_to_remove) and not (set(ticket.tags) & set(signal_tags)):
            oldestDate = ticketUpdated
            oldestID   = ticket.id

    oldestDate = oldestDate - datetime.timedelta(hours=4)
    hoursOpen  = countBusinessHours(oldestDate)

    # send to datadog
    logger.info("{}.oldestTicketID: {}".format(APP_NAME, oldestID))
    api.Metric.send(metric="{}.oldestTicketID".format(APP_NAME), points=(oldestID))
    logger.info("{}.hoursOpen: {}".format(APP_NAME, hoursOpen))
    api.Metric.send(metric="{}.hoursOpen".format(APP_NAME), points=(hoursOpen))

# ON HOLD TICKETS
def getOldestHold():
    onHoldTickets = zenpy_client.search(type='ticket', status='hold')

    oldestDate = now
    for ticket in onHoldTickets:
        tua = ticket.updated_at
        ticketUpdated = datetime.datetime(int(tua[0:4]), int(tua[5:7]), int(tua[8:10]), int(tua[11:13]), int(tua[14:16]), int(tua[17:19]))
        if (ticketUpdated < oldestDate) and (ticket.assignee_id not in agents_to_remove):
            oldestDate = ticketUpdated
            oldestID   = ticket.id

    # adjust for timezone
    oldestDate = oldestDate - datetime.timedelta(hours=4)
    hoursOpen  = countBusinessHours(oldestDate)

    # send to datadog
    logger.info("{}.oldestHoldTicketID: {}".format(APP_NAME, oldestID))
    api.Metric.send(metric="{}.oldestHoldTicketID".format(APP_NAME), points=(oldestID))
    logger.info("{}.hoursOnHold: {}".format(APP_NAME, hoursOpen))
    api.Metric.send(metric="{}.hoursOnHold".format(APP_NAME), points=(hoursOpen))

# Signal Support tickets
def getOldestSignalSupport():
    openAndNewTickets = zenpy_client.search(type='ticket', status_less_than='pending')

    oldestDate = now + datetime.timedelta(hours=4) # hacky but it works
    oldestID   = 1
    for ticket in openAndNewTickets:
        # Count number of ticket comments
        nComments = 0
        for audit in zenpy_client.tickets.audits(ticket.id):
            for event in audit.events:
                if event["type"] == 'Comment':
                    nComments = nComments + 1

        tua = ticket.updated_at
        ticketUpdated = datetime.datetime(int(tua[0:4]), int(tua[5:7]), int(tua[8:10]), int(tua[11:13]), int(tua[14:16]), int(tua[17:19]))
        # if the only comment is the initial one and its older then the current oldest and it's a support ticket, then this ticket is now oldest
        if nComments == 1 and (ticketUpdated < oldestDate) and (ticket.assignee_id not in agents_to_remove) and (set(ticket.tags) & set(signal_tags)):
            oldestDate = ticketUpdated
            oldestID   = ticket.id

    oldestDate = oldestDate - datetime.timedelta(hours=4)
    hoursOpen  = countBusinessHours(oldestDate)

    # send to datadog
    logger.info("{}.oldestSignalTicketID: {}".format(APP_NAME, oldestID))
    api.Metric.send(metric="{}.oldestSignalTicketID".format(APP_NAME), points=(oldestID))
    logger.info("{}.signalHoursOpen: {}".format(APP_NAME, hoursOpen))
    api.Metric.send(metric="{}.signalHoursOpen".format(APP_NAME), points=(hoursOpen))

if __name__ == '__main__':
    #getOldestOpenOrNew()
    getOldestOpenNoResponse()
    getOldestSignalSupport()
    getOldestHold()
