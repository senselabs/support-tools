import sys
from os.path import expanduser, exists
from os import makedirs
from requests import get, post
from getpass import getpass
from subprocess import Popen, PIPE, STDOUT
from pprint import pprint

auth_path = expanduser("~/.sense_auth")
auth_uri = "https://api.sense.com/api/v1/authenticate"
admin_base_url = "https://api.sense.com/api/v1/admin"


class Auth:
    def __user_auth(self):
        try:
            email = input("Admin Console Email: ")
            password = getpass(
                prompt="Please enter your password: ", stream=sys.stderr)
            payload = "email={}&password={}".format(email, password)
            headers = {
                'Content-Type': "application/x-www-form-urlencoded"
            }
            response = post(auth_uri, headers=headers, data=payload).json()
            auth_token = response["access_token"]
            return auth_token
        except KeyError:
            print("Credentials not accepted, please retry.")
            exit()

    def __service_auth(self, email, password):
        payload = "email={}&password={}".format(email, password)
        headers = {
            'Content-Type': "application/x-www-form-urlencoded"
        }
        response = post(auth_uri, headers=headers, data=payload).json()
        auth_token = response["access_token"]
        return auth_token

    def __store_token(self, token):
        with open(auth_path, 'w') as file:
            file.write(token)

    def load_token(self):
        with open(auth_path, 'r') as file:
            token = file.read()
            return token

    def get_token(self, require_input=True, email=None, password=None, header_form=False):
        if require_input:
            if exists(auth_path):
                token = self.load_token()
            else:
                self.__store_token(self.__user_auth())
                token = self.load_token()
        else:
            if exists(auth_path):
                token = self.load_token()
            else:
                self.__store_token(self.__service_auth(email, password))
                token = self.load_token()
        if header_form:
            token = {"Authorization": "bearer {}".format(token)}
        return token

    def gui_get(self, email, password):
        token = self.__store_token(self.__service_auth(email, password))
        self.test_auth(token)

    def test_auth(self, token=None):
        if token is None:
            token = self.load_token()
        headers = {"Authorization": "bearer {}".format(token)}
        r = get("https://api.sense.com/api/v1/admin/monitors/",
                headers=headers).status_code
        if r == 200:
            return True
        else:
            return False

    def remove_auth(self):
        if exists(auth_path):
            Popen(["rm", auth_path])
            return True
        else:
            return False


class Monitor:
    def __init__(self, monitor_id, auth):
        self.monitor_id = OrganizeHouse(monitor_id).unify
        self.auth = {"Authorization": "bearer {}".format(auth)}

    def get_timezone(self):
        url = "{}/monitors/{}".format(admin_base_url, self.monitor_id)
        resp = get(url, headers=self.auth).json()["time_zone"]
        return resp

    def update_timezone(self, timezone):
        url = "{}/monitors/{}/timezone".format(admin_base_url, self.monitor_id)
        payload = "timezone={}".format(timezone)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)

        resp = post(url, headers=headers, data=payload).status_code
        return resp

    def get_devices(self, device_id=None):
        url = "{}/monitors/{}/devices".format(admin_base_url, self.monitor_id)
        if device_id is not None:
            url = "{}/{}".format(url, device_id)
        resp = get(url, headers=self.auth).json()
        return resp

    def get_monitor_info(self, with_users=False, get_status=False):
        url = "{}/monitors/{}".format(admin_base_url, self.monitor_id)
        if get_status:
            url = "{}/status".format(url)
        if with_users:
            url = "{}?with_users=True".format(url)
        response = get(url, headers=self.auth).json()
        return response

    def get_gui_load(self):
        url = "{}/monitors/{}/status".format(admin_base_url, self.monitor_id)
        response = get(url, headers=self.auth)

        r = response.json()
        status = response.status_code

        output = {"monitor_serial": r["serial_number"],
                  "ethernet": str(r["ethernet_supported"]),
                  "install_date": r["date_created"],
                  "model_version": r["model_info"]["version"],
                  "model_date": r["model_info"]["release_date"],
                  "online_status": r["monitor_status"]["connected"],
                  "signal_check_status": r["signal_check_status"],
                  "solar": r["solar_configured"],
                  "timezone": r["time_zone"],
                  "monitorsw": r["sw_version"],
                  "signal_strength": "-" + str(int(r["status"]["wifi_strength"])) + " dBm",
                  "response_status": status}
        # TODO split install date to date and time

        return output

    def get_channelmap(self):
        url = "{}/monitors/{}/config".format(admin_base_url, self.monitor_id)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)
        r = get(url, headers=headers).json()
        output = r["channel_map"]
        return output

    def update_channelmap(self, parameter, value):
        url = "{}/monitors/{}/config".format(admin_base_url, self.monitor_id)
        payload = "channel_map%3B{}={}".format(parameter, value)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)
        r = post(url, headers=headers, data=payload)
        return r.status_code

    def restart_service(self, service):
        url = "{}/monitors/{}/restart_services".format(
            admin_base_url, self.monitor_id)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)
        r = post(url, headers=headers, data="services={}".format(service))
        return r.status_code

    def request_hello(self):
        url = "{}/monitors/{}/request_hello".format(
            admin_base_url, self.monitor_id)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)
        r = post(url, headers=headers)
        return r.status_code

    def update_config(self):
        url = "{}/monitors/{}/update_config".format(
            admin_base_url, self.monitor_id)
        r = post(url, headers=self.auth)
        return r.status_code

    def complete_solar_setup(self):
        url = "{}/monitors/{}/solar_setup_complete".format(
            admin_base_url, self.monitor_id)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        headers.update(self.auth)
        r = post(url, headers=headers)
        return r.status_code


class OrganizeHouse:
    def __init__(self, monitor_id):
        self.monitor_id = monitor_id
        self.thousands = self.thousands_placement()
        self.millions = self.millions_placement()
        self.unify = self.unify_house()


    def unify_house(self):
        monitor_id = self.monitor_id
        try:
            if monitor_id[0].lower() == "h":
                monitor_id = monitor_id[1:]
        except IndexError:
            pass
        return monitor_id

    def thousands_placement(self):
        house_id = self.unify_house()
        if len(house_id) == 4:
            placement = "t00{}".format(house_id[0])
        elif len(house_id) == 5:
            placement = "t0{}".format(house_id[0:2])
        elif len(house_id) == 6:
            placement = "t{}".format(house_id[0:3])
        elif len(house_id) > 6:
            placement = "t{}".format(house_id[2:5])
        else:
            placement = "t000"
        return placement

    def millions_placement(self):
        house_id = self.unify_house()
        if len(house_id) == 7:
            placement = "m00{}".format(house_id[0])
        elif len(house_id) == 8:
            placement = "m0{}".format(house_id[0:2])
        elif len(house_id) == 9:
            placement = "m{}".format(house_id[0:3])
        else:
            placement = "m000"
        return placement


class Buckets:
    # Used to easily access various S3 buckets for Support scripts
    def __init__(self, monitor_id, bucket):
        org = OrganizeHouse(monitor_id)
        self.monitor_id = org.unify
        self.millions_placement = org.millions
        self.thousands_placement = org.thousands
        self.bucket = bucket

    def get_bucket_location(self, device_type=None):
        # buckets would be either: combined-monitor-data || house-models
        base = "s3://{}/{}/{}/h{}".format(self.bucket,
                                          self.millions_placement, self.thousands_placement, self.monitor_id)
        if device_type is not None:
            self.bucket = "house-models"
            base = "{}/{}".format(base, device_type)
        return base

    def get_runs(self):
        self.bucket = "combined-monitor-data"
        location = self.get_bucket_location() + "/"
        command = ["aws", "s3", "ls", location]
        execute = Popen(command, stdout=PIPE)
        grep = Popen(["grep", "w60.gz"], stdin=execute.stdout,
                     stdout=PIPE).communicate()
        out = grep[0].decode("utf-8").split()
        end = ".w60.gz"
        start = "h{}.".format(self.monitor_id)

        collector = [x.strip(end).strip(start).strip("run")
                     for x in out if x[0:len(start)] == start]
        runs = []
        for run in collector:
            try:
                run = int(run)
                runs.append(run)
            except ValueError:
                pass
        
        # remove duplicates
        runs.sort()
        return list(dict.fromkeys(runs))


    def get_model_version(self, device=None):
        self.bucket = "house-models"
        if device is not None:
            location = self.get_bucket_location(device) + "/"
        else:
            location = self.get_bucket_location() + "/prod/"
        command = ["aws", "s3", "ls", location]
        execute = Popen(command, stdout=PIPE)
        grep = Popen(["grep", "models"], stdin=execute.stdout,
                     stdout=PIPE).communicate()
        out = grep[0].decode("utf-8").split()
        start = "models.V"
        end = "/"
        collector = [x.strip(end).strip(start)
                     for x in out if x[0:len(start)] == start]

        versions = []
        for version in collector:
            try:
                versions.append(int(version))
            except ValueError:
                pass
        return versions

    def get_detector_log(self, detector, version):
        location = self.get_bucket_location(device_type=detector)
        location = location + "/models.V{}/detector.log".format(version)
        return location


class Utility:

    def check_file_exists(self, filename, create=False):
        if exists(filename):
            return True
        else:
            if create:
                command = ["touch", filename]
                execute = Popen(command)
                return True
        return False

    def check_dir_exists(self, dir_location, create=False):
        if exists(dir_location):
            return True
        else:
            if create:
                makedirs(dir_location)
                return True
        return False

Buckets("h183388", " ").get_runs()