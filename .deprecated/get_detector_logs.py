from SupportTools.Support import Buckets, Utility
from os.path import expanduser, exists
from os import makedirs
import argparse
from subprocess import Popen, PIPE


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("monitor_id", type=str)
    parser.add_argument("detector", type=str)
    parser.add_argument("model_version",
                        type=str, default=None, nargs="?")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_opts()
    bucket = Buckets(args.monitor_id, "house-models")
    location = None
    if args.model_version is None:
        model_version = max(bucket.get_model_version(device=args.detector))
        location = bucket.get_detector_log(args.detector, model_version)
    else:
        model_versions = bucket.get_model_version(device=args.detector)
        if args.model_version in model_versions:
            location = bucket.get_detector_log(
                args.detector, args.model_version)
        else:
            print("Model version: {} not found for detector: {}".format(
                args.model_version, args.detector))
    if location is not None:
        command = ["aws", "s3", "cp", location, "-"]
        execute = Popen(command, stdout=PIPE)
        execute_2 = Popen(["less"], stdin=execute.stdout)
