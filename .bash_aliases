# Activate support virtualenv
source /data/shared-support/.virtualenvs/latest/bin/activate
alias py=/data/shared-support/.virtualenvs/latest/bin/python

# Aliases
alias pushSC='py /data/shared-support/code/support-tools/tools/signal_check/forceChannelCheck.py'
alias sshReMapper='ssh -i ~/.ssh/id_rsa ubuntu@remapper.ds.sense.com'
alias reMapper='/data/shared-support/code/tools/p4.0/CM/runMapperNewCL.cmd'
alias updateConfigManually='py /data/shared-support/code/pytools/pytools/tools/support/monitor_config.py'
alias displayHouse='/data/shared-support/code/support-tools/tools/visualize_data/displayHouseMore.cmd'
alias runTestMapper='/data/shared-support/code/tools/p4.0/CM/mapperTest/runMapperTest.cmd'
alias getChannelMapFailures='py /data/shared-support/code/pytools/pytools/tools/platform/apitool.py get_channelmap_support_requests'
alias closeChannelMapRequest='py /data/shared-support/code/pytools/pytools/tools/platform/apitool.py close_channelmap_support_request'
alias updateChannelMap='py /data/shared-support/code/support-tools/tools/signal_check/updateChannelMap.py'
alias monLogs='py /data/shared-support/code/support-tools/tools/connections/monbt.py'
alias modelInfo='py /data/shared-support/code/support-tools/tools/device_data/lsSm.py'
alias s3ls='py /data/shared-support/code/support-tools/tools/device_data/s3ls.py'
alias printLogForHouse='py /data/shared-support/code/support-tools/tools/device_data/printLogForHouse.py'
alias gen3ModelInfo='/data/shared-support/.virtualenvs/py2/bin/python /data/shared-support/code/pytools/pytools/tools/support/gen3_model_info.py'
alias mops="py /opt/sense/monitorsw/src/tools/mops.py"
alias pytoolsAuth='py /data/shared-support/code/pytools/sense_api_client/sense_api_client/utils/store_platform_credentials.py'
alias getNetworkTests='py /data/shared-support/code/support-tools/tools/connections/get_network_tests.py'
alias associateMonitorToEmail='py /data/shared-support/code/support-tools/tools/config/assign_email.py'
alias fallbackSearchCompare='/data/shared-support/.virtualenvs/py2/bin/python /data/shared-support/code/pytools/pytools/tools/viz/fallback_searchcompare.py'
alias fallbackSSH='/data/shared-support/code/sage_puppet/tools/ssh_monitor.sh'
alias clearMapperHouses='/data/shared-support/code/tools/p4.0/CM/clearMapperWorkingForUser.cmd $USER'
alias rmMapperCore='rm /data/shared-support/code/tools/p4.0/CM/core'
alias runCluster='/data/shared-support/code/support-tools/tools/device_data/runCluster.sh'
alias fixLG='/data/shared-support/.virtualenvs/shared-support/bin/python3 /data/shared-support/code/support-tools/tools/workarounds/lg_mapper_fix/main.py'
alias sample_adc="py3 /data/shared-support/code/support-tools/tools/visualize_data/sample_adc.py"
alias sendMonthlyReport="/opt/sense/venv/bin/python /opt/sense/support-tools/tools/reporting/send_monthly_report.py"
alias scatter='py /home/anthony/code/pytools/pytools/tools/device/run_scatter.py --no-l'
alias flickerd='/opt/sense/tools/research/flicker/d'
alias updateTZ='py -W ignore /data/shared-support/code/support-tools/tools/config/update_timezone.py'

# Deprecated Aliases
#alias runTestMapper='~/code/support-tools/tools/training/test_mapper/runTestMapper.cmd'
#alias checkMergedDevices='python3 /data/shared-support/code/support-tools/tools/device_data/checkMergedDevices.py'

function wattsurfer {
    py /data/shared-support/code/pytools/pytools/tools/device/run_wattsurfer_for_gen3.py --work-dir ~/ $@ 
}

# Functions

function searchCompare {
	house=$1
	py /data/shared-support/code/pytools/pytools/tools/viz/searchcompare.py $house --cache-dir /data/shared-support/cache-data/$USER/
}

#function voltageSurfer {
#    py /data/shared-support/code/pytools/pytools/tools/device/run_wattsurfer_for_gen3.py $1 --show-log-voltage --volt-multiply 38
#}

function voltageSurfer {
    wattsurfer --show-log-voltage --volt-multiply 38 $@
}
