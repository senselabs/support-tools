import argparse
from Sense.Organize import OrganizeHouse
from subprocess import check_output

def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house")
    return parser.parse_args()

def fix(house):
    cmd = ["/data/shared-support/.virtualenvs/shared-support/bin/aws","s3", "cp", "/data/shared-support/code/support-tools/tools/workarounds/lg_mapper_fix/sample.lg", f"s3://house-models/{house.millions}/{house.thousands}/h{house.unify_house()}/CM/labelGroups/h{house.unify_house()}.lg"]
    response = check_output(cmd).decode("utf-8")
    print(response)
if __name__ == '__main__':
    args = parse_opts()
    house = OrganizeHouse(args.house)
    try:
        fix(house)
        print("Updated labelGroup file, run Mapper")
    except:
        print("Failed to update labelGroup..")
