
from Sense.Api import Auth, Monitor
import argparse
from pprint import pprint

line_break = "------------------------------------------------------------------------------------------------"


def parse_ops():
    parser = argparse.ArgumentParser()
    parser.add_argument("monitor")
    parser.add_argument("--result", dest="result",
                        default=None, choices=["p", "w", "f"])
    parser.add_argument("--latest", dest="latest",
                        default=False, action="store_true")
    return parser.parse_args()


def get_tests(monitor_id):
    collector = []
    monitor = Monitor(monitor_id, Auth().token)
    tests = monitor.get_network_tests()
    for test in tests:
        collector.append(test)
    return collector


def parse_tests(args, tests):
    collector = []
    if args.result is not None:
        for test in tests:
            if test["result"] == args.result:
                collector.append(test)
    elif args.result:
        collector.append(tests[-1])
    else:
        for test in tests:
            collector.append(test)
    return collector


def main():
    args = parse_ops()
    tests = get_tests(args.monitor)
    if args.latest:
        print(line_break)
        pprint(tests[-1])
        print(line_break)
    if args.result is not None:
        for test in tests:
            if test["result"] == args.result:
                print(line_break)
                pprint(test)
                print(line_break)
    else:
        for test in tests:
            print(line_break)
            pprint(test)
            print(line_break)


main()
