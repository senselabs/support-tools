#!/usr/bin/env python
from Sense.Api import Auth, Monitor
import argparse
from pprint import pprint


def parse_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument("monitor_id")
    parser.add_argument("--date", dest="date", default=None)
    parser.add_argument("--setup-result", dest="setup_result", default=None)

    return parser.parse_args()

def main():
    args = parse_opts()
    monitor = Monitor(args.monitor_id, Auth().token)
    result = monitor.get_setup_logs(date=args.date, setup_result=args.setup_result)

    pprint(result)

main()