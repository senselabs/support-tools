__author__ = 'shakwan'

import datetime
from zenpy import Zenpy
from datadog import initialize, api
import time
import logging
#from pprint import pprint as print
import re
import csv
from collections import Counter

APP_NAME = 'zendesk'

creds = {
    'email' : 'shakwan@sense.com',
    'token' : '2fJhRZltjqQPmviZHthQ7YkOzLee8q0E05LFeUyr',
    'subdomain': 'senseapp'
}


options = {
    'api_key': 'ee1ac599620f1efb82c8bb20ac9ebd5d',
    'app_key': 'b5bc836e96a2fd18e2bc6eeadc006bb62c9a87b1'
}

agents_to_remove = [
    'App Support',
    'Rhoda Ullmann',
    'Bradford Swanson',
    'Jen Stevenson',
    'Josh',
    'Audrey Steeves',
    'Ryan',
    'George',
    'Shakwan Burnett',
    'Margot B',
    'Aisha Strauss'
]
statuses = [
    'open',
    'hold',
    'pending'
]

reasons = [
    'tech_issues__device_detection',
    'tech_issues__signal',
    'channel__',
    'tech_issues__wifi__',
    'tech_issues__bluetooth',
    'tech_issues__device_behavior',
]
prefixes = [
    'tech_issues__',
    'channel__',
    'feedback__',
    'question__',
    'shipping__',
    'returns__',
    'order__',
    'general'
]

requesters = [
    'willson@grandetexas.net',
    'unibass@gmail.com',
    'nshweb2002@yahoo.com',
    'robert.means1969@gmail.com',
    'randy@gawer.org',
    'michael@michaeljgorman.com',
    'tejaycar@gmail.com',
    'josh@kenyoncomputer.com',
    'christopher.olson@gmail.com',
    'lukecure@gmail.com',
    'johndellarciprete@gmail.com',
    'martin.scheyhing@gmail.com',
    'mark@mark-etal.com',
    'andrewdcaudle@gmail.com',
    'ecushman@gmail.com',
    'gbfrozen@gmail.com',
    'mike@thezieglers.org',
    'dgoldy56md@gmail.com'
]

initialize(**options)

now = int(time.time())

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


zenpy_client = Zenpy(**creds)
requestsStart = datetime.datetime.now() - datetime.timedelta(days=7)
yesterday     = datetime.datetime.now() - datetime.timedelta(days=2)
agents = []

def sendMetrics(metric_name, metric_value, host ):
    print(metric_name)
    if host:
        api.Metric.send(metric="open_tickets", points=(now, metric_value), host=host)
    else:
        api.Metric.send(metric="{}.{}".format(APP_NAME, metric_name), points=(now, metric_value))

def getTicketAgent():
    for user in zenpy_client.search(role="agent"):
        agents.append(user.name)
    for user in zenpy_client.search(role="admin"):
        agents.append(user.name)
    for agent in agents_to_remove:
        agents.remove(agent)
    return agents

def getAgentTicketCount(agents):
    for agent in agents:
        numTickets = 0
        agentTicketCount = []
        firstName = agent.split(" ")[0]
        agent_names = []
        agent_names.append(firstName)
        for ticket in zenpy_client.search(type='ticket', status='open', assignee=agent):
            agentTicketCount.append(ticket)
            numTickets = len(agentTicketCount)
            if numTickets == None:
                numTickets   = 0
                print(numTickets)
        for agent_name in agent_names:
            agent_name = agent_name.lower()
            logger.info("Sending: {}_{}.open_tickets {}".format(APP_NAME, agent_name, numTickets))
            api.Metric.send(metric="{}.open_tickets".format(APP_NAME) , points=(now, numTickets), host=agent_name)

def getTagNames():
    ticket_tags = zenpy_client.tags()
    for tag in ticket_tags.values:
        for prefix in prefixes:
            if re.search(prefix, tag['name']):
                print(tag['name'])

def getTicketsByTag():
    ticket_tags = zenpy_client.tags()
    for tag in ticket_tags.values:
        for reason in reasons:
            if re.search(reason, tag['name']):
                numTickets = len(zenpy_client.search(type='ticket', tags=tag['name'], created_after=requestsStart))
                print("{}:{}".format(tag['count'], numTickets))
                #logger.info("Sending {}.{}_open_tickets : {}".format(APP_NAME, tag['name'], numTickets))
        #api.Metric.send(metric="{}.{}_open_tickets".format(APP_NAME, reportTag['name']), points=(now, numTickets))

def getTicketCountByGroup():
    ticket_groups = zenpy_client.groups()
    for group in ticket_groups.values:
        groupTicketCount = zenpy_client.search(type='ticket', group=group['name'], status='open')
        numTickets = len(groupTicketCount)
        groupName = group['name']
        logger.info("{}_{}_open_tickets : {}".format(APP_NAME, groupName, numTickets))
        api.Metric.send(metric="{}.{}_open_tickets".format(APP_NAME, groupName), points=(now, numTickets))

def getTotalTicketCounts():
    for status in statuses:
        numTickets = len(zenpy_client.search(type='ticket', status=status))
        logger.info("{}.{}_tickets : {}".format(APP_NAME, status, numTickets))
        api.Metric.send(metric="{}.{}_tickets".format(APP_NAME, status), points=(now, numTickets))

def getTicketsByEmail():
    with open('tickets.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(['Requester', 'Subject', 'Description', ])
        for requester in requesters:
            tickets = zenpy_client.search(type='ticket', requester=requester)
            for ticket in tickets.values:
                if re.search('Order', ticket['subject']):
                    pass
                else:
                    print("Requester: {}".format(requester))
                    print("Subject: {}".format(ticket['subject']))
                    print("Description: {}".format(ticket['description']))
                    writer.writerow([requester,ticket['subject'],ticket['description']])
    file.close()

def getCurrentTopReasons():
    reasons     = []
    openTickets = zenpy_client.search(type='ticket', status='open')

    for ticket in openTickets:
        # ignore tickets without a reason code set
        if isinstance(ticket.custom_fields[3]['value'], str):
            thisReason = ticket.custom_fields[3]['value']
            reasons.append(thisReason)

    c = Counter(reasons)
    mostCommon = c.most_common(5)
    for reason in mostCommon:
        logger.info("{}.currentTopReasons: {}: {}".format(APP_NAME, reason[0], reason[1]))
        api.Metric.send(metric="{}.currentTopReasons".format(APP_NAME), points=(now, reason[1]), host = (reason[0]))

def getTrendingReasons():
    reasons         = []
    lastWeekTickets = zenpy_client.search(type='ticket', created_after=requestsStart)

    for ticket in lastWeekTickets:
        # ignore tickets without a reason code set
        if isinstance(ticket.custom_fields[3]['value'], str):
            thisReason = ticket.custom_fields[3]['value']
            reasons.append(thisReason)

    c = Counter(reasons)
    mostCommon = c.most_common(5)
    for reason in mostCommon:
        logger.info("{}.trendingReasons: {}: {}".format(APP_NAME, reason[0], reason[1]))
        api.Metric.send(metric="{}.trendingReasons".format(APP_NAME), points=(now, reason[1]), host = (reason[0]))

def totalTicketsLastWeek():
    lastWeekTickets = zenpy_client.search(type='ticket', created_after=requestsStart)
    nTickets        = len(lastWeekTickets)
    logger.info("{}.totalTicketsLastWeek: {}".format(APP_NAME, nTickets))
    api.Metric.send(metric="{}.totalTicketsLastWeek".format(APP_NAME), points=(now, nTickets))
    """
    days = []
    for ticket in lastWeekTickets:
        days.append(ticket.created_at[5:10])

    c = Counter(days)
    dailyCounts = dict(c)

    for day, number in sorted(dailyCounts.items()):
        logger.info("{}.totalTicketsLastWeekTEST: {}: {}".format(APP_NAME, day, number))
        api.Metric.send(metric="{}.totalTicketsLastWeekTEST".format(APP_NAME), points=(day, number), host = day)
    """

def solvedTicketsLastWeek():
    lastWeekSolved = zenpy_client.search(type='ticket', solved_after=requestsStart)
    nTickets       = len(lastWeekSolved)
    logger.info("{}.lastWeekSolved: {}".format(APP_NAME, nTickets))
    api.Metric.send(metric="{}.lastWeekSolved".format(APP_NAME), points=(now, nTickets))

def totalTouchesLastWeek():
    nTouches        = 0
    lastWeekTickets = zenpy_client.search(type='ticket', created_after=requestsStart)

    for ticket in lastWeekTickets:
        for thisAudit in zenpy_client.tickets.audits(ticket.id):
            if thisAudit.events[0]['type'] == 'Comment':
                nTouches = nTouches + 1

    logger.info("{}.totalTouchesLastWeek: {}".format(APP_NAME, nTouches))
    api.Metric.send(metric="{}.totalTouchesLastWeek".format(APP_NAME), points=(now, nTouches))


if __name__ == '__main__':
    #getTagNames()
    #getTicketsByTag()
    #getTicketsByEmail()
    getTotalTicketCounts() #uncomment below
    #getTicketCountByGroup()
    getAgentTicketCount(getTicketAgent())
    getCurrentTopReasons()
    getTrendingReasons()
    totalTicketsLastWeek()
    solvedTicketsLastWeek()
    totalTouchesLastWeek()
