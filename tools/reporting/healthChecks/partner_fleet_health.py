from pprint import pprint
from sense_support_api.auth import header_auth
from sense_support_api.partner_api import *
from time import sleep


def collect_partners():
    partner_data = {}
    all_partners = get_partner()
    for partner in all_partners:
        partner_id = partner["id"]
        partner_name = partner["name"]
        partner_data.update({partner_id: partner_name})
    return partner_data
