from os import name, kill
from multiprocessing import Process
import subprocess
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject, pyqtSignal
from pprint import pprint
from time import sleep
import signal
import sys
import gui
from time import sleep
from Sense.Api import Auth, Monitor

from Sense.AwsUtils import Buckets
import subprocess
from datetime import datetime
from os.path import expanduser, exists
import logging
from json import dumps
from dateutil import parser
from getpass import getuser

logging.basicConfig(filename='log.log', level=logging.INFO)

PY2_EXEC = "/data/shared-support/.virtualenvs/py2/bin/python"
PY3_EXEC = "/data/shared-support/.virtualenvs/shared-support/bin/python"
USER = getuser()

wattsurfer_procs = []


class MainWindow(QMainWindow, gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        error_message = QMessageBox()
        self.error_message = error_message

        buttons = [self.runWattsurfer, self.stopWattsurfer, self.clearWattsurferBtn, self.runSearchCompareBtn, self.stopSearchCompareBtn, self.clearSearchCompareOutputBtn, self.runGen3Btn, self.clearGen3ModelInfoBtn,
                   self.printLogForHouseBtn, self.clearPrintLogForHouseBtn, self.mergedDeviceLoad, self.mergedDeviceClearOutput, self.s3lsClearOutput, self.runS3LS, self.printModelListBtn, self.printModelClearBtn, self.printModelReadBtn]
        # Disable wattsurfer tools until working
        for button in buttons:
            if button.clicked.connect(self.PageListener):
                pass

    def PageListener(self):
        self.house_number = self.houseNumber.text()
        listener = self.sender()
        self.matcher = listener.objectName()
        if len(self.matcher) > 1:
            msg = f"{USER} performed: {self.matcher}"
            if self.house_number is not None:
                msg = msg + f" on monitor: {self.house_number}"
            else:
                msg = msg + "without a house number..."
            print(msg)
        print(self.matcher)
        if self.matcher == "printLogForHouseBtn":
            self.printLogForHouseBtn.setEnabled(False)
            self.runPrintLogForHouse()
            self.printLogForHouseBtn.setEnabled(True)
        if self.matcher == "clearPrintLogForHouseBtn":
            self.clearPrintLogForHouse()
        if self.matcher == "runWattsurfer":
            self.wattsurfer_process = Process(target=self.runWattsurferExe)
            # wattsurfer_procs.append(self.wattsurfer_process)
            print("Started")
            self.wattsurfer_process.start()
            sleep(0.5)
            wattsurfer_procs.append(self.wattsurfer_process)
            for pid in wattsurfer_procs:
                print(f"Wattsurfer: {pid}")
        if self.matcher == "stopWattsurfer":
            for process in wattsurfer_procs:
                print(f"Killing PID: {process.pid}")
                sleep(1)
                kill(process.pid, signal.SIGTERM)
                # process.terminate()
                wattsurfer_procs.remove(process)
                # process.send_signal(signal.SIGTERM)
            pass
        if self.matcher == "mergedDeviceLoad":
            self.runGetMergedDevices()
        if self.matcher == "mergedDeviceClearOutput":
            self.clearMergedDeviceOutput()
        if self.matcher == "runS3LS":
            self.executeS3LS()
        if self.matcher == "s3lsClearOutput":
            self.clearS3LS()
        if self.matcher == "runGen3Btn":
            self.executeGen3ModelInfo()
        if self.matcher == "clearGen3ModelInfoBtn":
            self.clearGen3ModelInfoOutput()
        if self.matcher == "printModelListBtn":
            self.listModelInfo()
        if self.matcher == "printModelReadBtn":
            self.printSelectedModel()
        if self.matcher == "printModelClearBtn":
            self.printModelListOutput.clear()
            self.printModelReadOutput.setText("")

    def clearPrintLogForHouse(self):
        self.textBrowser_2.setText("")

    def executeS3LS(self):
        self.s3lsOutputTable.setRowCount(0)
        only = None
        mapper = {"date": 0,
        "house": 1,
        "run": 2,
        "file_type": 3}
        counter = 0 
        try:
            output = Buckets(self.house_number).get_monitor_files()
            if self.s3lsW60Only.isChecked():
                only = "w60"
            if self.s3lsS60Only.isChecked():
                only = "s60"
            if self.s3lsLogFilesOnly.isChecked():
                only = "log"
            for line in output:
                if "/" not in line: # This is to prevent the directories from showing.
                    line = line.split(" ")
                    date = line[0]
                    data = line[-1]
                    data = data.split(".")
                    house = data[0]
                    run = data[1]
                    try:
                        data_type = data[2]
                        if only:
                            if only in data_type:
                                if data_type == "log":
                                    self.s3lsOutputTable.insertRow(counter)
                                    self.s3lsOutputTable.setItem(counter, mapper["date"], QTableWidgetItem(date))
                                    self.s3lsOutputTable.setItem(counter, mapper["house"], QTableWidgetItem(house))
                                    self.s3lsOutputTable.setItem(counter, mapper["run"], QTableWidgetItem(run))
                                    self.s3lsOutputTable.setItem(counter, mapper["file_type"], QTableWidgetItem(data_type))
                                    counter += 1
                                elif "sections" not in data_type and data_type != "loglist":
                                    self.s3lsOutputTable.insertRow(counter)
                                    self.s3lsOutputTable.setItem(counter, mapper["date"], QTableWidgetItem(date))
                                    self.s3lsOutputTable.setItem(counter, mapper["house"], QTableWidgetItem(house))
                                    self.s3lsOutputTable.setItem(counter, mapper["run"], QTableWidgetItem(run))
                                    self.s3lsOutputTable.setItem(counter, mapper["file_type"], QTableWidgetItem(data_type))
                                    counter += 1

                        else:
                            self.s3lsOutputTable.insertRow(counter)
                            self.s3lsOutputTable.setItem(counter, mapper["date"], QTableWidgetItem(date))
                            self.s3lsOutputTable.setItem(counter, mapper["house"], QTableWidgetItem(house))
                            self.s3lsOutputTable.setItem(counter, mapper["run"], QTableWidgetItem(run))
                            self.s3lsOutputTable.setItem(counter, mapper["file_type"], QTableWidgetItem(data_type))
                            counter += 1
                    except IndexError:
                        pass
        except TypeError as e:
            print(e)
            self.error_message.setText("House number is not valid.")
            self.error_message.show()

    def clearS3LS(self):
        self.s3lsOutputTable.setRowCount(1)
        
    def executeGen3ModelInfo(self): 
        if "h" not in self.house_number:
            self.house_number = f"h{self.house_number}"
        self.gen3TableOutput.setRowCount(0)
        loc = "/data/shared-support/code/pytools/pytools/tools/support/gen3_model_info.py"
        exe_cmd = [PY2_EXEC, loc, self.house_number]
        print(self.gen3ModelVersion.text())
        if len(self.gen3ModelVersion.text()) >= 1:
            if self.allModelVersions.isChecked():
                self.error_message.setText("Cannot specify model version and also have 'All Model Versions' checked.")
                self.error_message.show()
                return
            else:
                exe_cmd.append("--version")
                exe_cmd.append(self.gen3ModelVersion.text().strip())
        if self.showDeviceNames.isChecked():
            exe_cmd.append("--show")
            exe_cmd.append("db")
        if self.allModelVersions.isChecked():
            exe_cmd.append("--version")
            exe_cmd.append("-1")

        cmd = subprocess.Popen(exe_cmd, stdout=subprocess.PIPE)
        output, error = cmd.communicate()
        output = output.decode("utf-8")
        output = format_gen3(output, self.showDeviceNames.isChecked())
        counter = 0 
        device_mapper = {"Display Name": 0,
                         "Device ID": 1,
                         "Filename": 2,
                         "Device Type": 3,
                         "Detector": 4,
                         "Model Version Detected": 5,
                         "Status": 6}
        for model_version, data in output.items():
            self.gen3TableOutput.insertRow(counter)
            for i in range(9):
                self.gen3TableOutput.setItem(counter, i, QTableWidgetItem("______________________"))
            pprint
            new = data["New"]
            updated = data["Updated"]
            revoked = data["Revoked"]
            created = easy_dates(data["Created"])
            counter += 1  
            self.gen3TableOutput.insertRow(counter)
            # Set Model Version Header
            self.gen3TableOutput.setItem(counter, 0, QTableWidgetItem("Model Version:"))
            self.gen3TableOutput.setItem(counter, 1, QTableWidgetItem(model_version))
            self.gen3TableOutput.setItem(counter, 3, QTableWidgetItem("Date Deployed:"))
            self.gen3TableOutput.setItem(counter, 4, QTableWidgetItem(created))

            counter += 1  
            self.gen3TableOutput.insertRow(counter)
            self.gen3TableOutput.setItem(counter, 0, QTableWidgetItem("New Models:"))
            self.gen3TableOutput.setItem(counter, 1, QTableWidgetItem(new))
            # counter += 1  
            # self.gen3TableOutput.insertRow(counter)
            self.gen3TableOutput.setItem(counter, 2, QTableWidgetItem("Updated Models:"))
            self.gen3TableOutput.setItem(counter, 3, QTableWidgetItem(updated))
            # counter += 1  
            # self.gen3TableOutput.insertRow(counter)
            self.gen3TableOutput.setItem(counter, 4, QTableWidgetItem("Revoked Models:"))
            self.gen3TableOutput.setItem(counter, 5, QTableWidgetItem(revoked))
            header = ["Device Name:", "Device ID:", "File Name:", "Device Type:", "Detector:", "MV Detected:", "Status:"]

            counter += 1  
            self.gen3TableOutput.insertRow(counter)
            counter += 1  
            self.gen3TableOutput.insertRow(counter)
            for i, col in enumerate(header):
                self.gen3TableOutput.setItem(counter, i, QTableWidgetItem(col))
            counter += 1
            self.gen3TableOutput.insertRow(counter)


            for model in data["Models"]:
                for key, value in model.items():
                    row = device_mapper[key]
                    if type(value) == list:
                        value = str(value)
                    self.gen3TableOutput.setItem(counter, row, QTableWidgetItem(value))
                counter += 1
                self.gen3TableOutput.insertRow(counter)

            counter += 1  
            self.gen3TableOutput.insertRow(counter)



    def clearGen3ModelInfoOutput(self):
        self.gen3TableOutput.setRowCount(0)


    def runGetMergedDevices(self):
        self.mergedDeviceOutput.setText("")
        if self.mergedDeviceID.text() == "":
            self.error_message.setText("Please input a Device ID")
            self.error_message.show()
            return
        loc = "/data/shared-support/code/support-tools/tools/device_data/checkMergedDevices.py"
        cmd = subprocess.Popen([PY3_EXEC, loc, self.house_number, self.mergedDeviceID.text()], stdout=subprocess.PIPE)
        output, errors = cmd.communicate()
        output = output.decode("utf-8")
        if "does not exist" in output:
            self.error_message.setText(output)
            self.error_message.show()
            return
        else:
            output = output.replace(":", ":\n").replace(",", "\n")
        self.mergedDeviceOutput.append(output)

    def clearMergedDeviceOutput(self):
        self.mergedDeviceOutput.setText("")

    def listModelInfo(self):
        self.models = []
        loc = "/data/shared-support/code/support-tools/tools/device_data/lsSm.py"
        cmd_exe = [PY3_EXEC, loc, self.house_number, "ls"]
        cmd = subprocess.Popen(cmd_exe, stdout=subprocess.PIPE)
        output, error = cmd.communicate()
        output = output.decode("utf-8")
        for line in output.split("\n"):
            print(line)
            self.models.append(line)
            self.printModelListOutput.addItem(line)

    def printSelectedModel(self):
        self.printModelReadOutput.setText("")
        selected = self.printModelListOutput.currentRow()
        loc = "/data/shared-support/code/support-tools/tools/device_data/lsSm.py"
        cmd_exe = [PY3_EXEC, loc, self.house_number, "cat", self.models[selected]]
        cmd = subprocess.Popen(cmd_exe, stdout=subprocess.PIPE)
        output, error = cmd.communicate()
        output = output.decode("utf-8")
        self.printModelReadOutput.setText(output)


        

    def runPrintLogForHouse(self):
        self.textBrowser_2.setText("")
        loc = "/data/shared-support/code/support-tools/tools/device_data/printLogForHouse.py"
        cmd =[PY3_EXEC, loc, self.house_number] 
        if len(self.printLogForHouseSpecificRun.text()) >= 1:
            cmd.append("--run")
            cmd.append(self.printLogForHouseSpecificRun.text())
        cmd_exe = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        output, errors = cmd_exe.communicate()
        output = output.decode("utf-8")
        self.textBrowser_2.append(output)
    
    def runWattsurferExe(self):
        self.wattsurferConsole.setText("")
        loc = "/data/shared-support/code/pytools/pytools/tools/device/run_wattsurfer_for_gen3.py"
        cmd = subprocess.Popen([PY2_EXEC, loc, self.house_number], stdout=subprocess.PIPE)
        output = cmd.communicate()[0].decode("utf-8")
        self.wattsurferConsole.append(output)

        # console_proc = Process(target=self.wattsurfer_console, args=(output,))
        # console_proc.start()

    
    def wattsurfer_console(self, output):
        self.wattsurferConsole.append(output)

def easy_dates(date):
    date = parser.parse(date)
    date_str = date.strftime("%m/%d/%y %I:%M:%S %Z")
    return date_str


def wait(run, btn):
    while run.is_alive():
        sleep(0.5)
        btn.setEnabled(True)

def format_gen3(output, showDeviceNames=False):
    new_version_dash = "--------------------------------------------------------------------------------------------------------"
    start_model_list = "----------------  -----------  ---------------------  ---------------------------------  -------------------  ---------  --------"
    final = {}
    output = output.split("\n")
    # print(output)
    model_header = ["Display Name", "Device ID", "Filename", "Device Type", "Detector", "Model Version Detected", "Status"]
    version = None 
    for i, line in enumerate(output):
        if "--" not in line:
            if "Version " in line and "Status" not in line:
                version = line.replace("Version ", "")
                final.update({version: {"Version": version}})

            if "Created" in line:
                created_date = line.replace("Created: ", "")
                if version:
                    final[version].update({"Created": created_date})
            if "New" in line:
                line = line.split("\t")
                if version:
                    new_models = line[0].replace("New:", "")
                    updated_models = line[1].replace("Updated: ", "")
                    revoked_models = line[2].replace("Revoked: ", "")
                    final[version].update({"New": new_models,
                                            "Updated": updated_models,
                                            "Revoked": revoked_models,
                                            "Models": []})
                else:
                    print("Version is none")
            if "new" in line or "updated" in line or "persisted" in line or "revoked" in line:
                line = line.split(" ")
                line = [each for each in line if each != ""]
                # print(line)
                # print(len(line))
                if len(line) == 7:
                    display_name = line[0]
                    device_id = line[1]
                    filename = line[2]
                    device_type = line[3]
                    detector = line[4]
                    deployed_version = line[5]
                    try:
                        status = line[6]
                    except IndexError:
                        status = "None"
                elif len(line) == 8:
                    name_one = line[0]
                    name_two = line[1]
                    display_name = name_one + " " + name_two
                    device_id = line[2]
                    filename = line[3]
                    device_type = line[4]
                    detector = line[5]
                    deployed_version = line[6]
                    try:
                        status = line[7]
                    except IndexError:
                        status = "None"
                if version:
                    final[version]["Models"].append({"Display Name": display_name,
                                                            "Device ID": device_id,
                                                            "Filename": filename,
                                                            "Device Type": device_type,
                                                            "Detector": detector,
                                                            "Model Version Detected": deployed_version,
                                                            "Status": status})
            if showDeviceNames:
                if "NO" in line:
                    line = line.split(" ")
                    line = [each for each in line if each != ""]
                    if line[0] not in ["solar", "unknown", "always_on"]:
                        print(line)
                        device_id = line[0]
                        original_name = line[1]

    return final


        





if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
