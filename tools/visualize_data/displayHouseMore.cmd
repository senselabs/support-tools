#!/bin/tcsh -f

if ( $# < 1 ) then
  echo usage: displayHouse.cmd house '[run]'
  exit
endif


set profileStr = ""
set profile = `printenv S3_PROFILE`
if ( "$profile" != "" ) then
   set profileStr = "--profile $profile"
endif

set house = $1

set s3hdir = `$HOME/code/tools/p4.0/getHouseDirFromHouseName/getHouseDirFromHouseName $house`

set cdir = s3://combined-monitor-data/$s3hdir/$house

s3 ls $cdir/ | grep w60 | egrep -v sections

if ( $# > 1 ) then
  set w60 = $house.$2.w60.gz
else
  foreach w60(`s3 ls $cdir/ | awk '{print $4}' | grep "w60\." | sort -rn -tn -k+2`)
endif



if ( "$w60" == "" ) then
  echo cannot find data for $house
  exit
endif
 
echo w60 $w60

set hr = $w60:r:r

set s60 = $hr.s60.gz

set hasS60 = `s3 ls $cdir/$s60 | wc -l`

echo hasS60 $hasS60

s3 cat $cdir/$hr.log.gz | zcat | grep CONFIG | egrep '(SWAPPED|BACKWARDS|BREAKER|GAIN|USING|SOLAR|CHANNELS|serial|hw_version)'

if ( $hasS60 > 0 ) then
  ~/code/tools/p3.0/QT/wattsurfer/wattsurfer $cdir/$hr.w60.gz $cdir/$hr.s60.gz -x 20 -y 20 -w 2400 -h 1200 -c $HOME/code/tools/p4.0/supportTools/w60s60.config
else 
  ~/code/tools/p3.0/QT/wattsurfer/wattsurfer $cdir/$hr.w60.gz -x 20 -y 20 -w 2400 -h 1200 -c $HOME/code/tools/p4.0/supportTools/w60.config
endif

end
