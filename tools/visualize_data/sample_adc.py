import pexpect
from pexpect.exceptions import TIMEOUT
import argparse
from time import sleep

INTERPRETER_PATH="/data/shared-support/.virtualenvs/shared-support/bin/python"

def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("serial")
    return parser.parse_args()

def run(serial):
    mops = pexpect.spawn(f"{INTERPRETER_PATH} /data/shared-support/code/monitorsw/src/tools/mops.py {serial} ssh")
    print(f"Connecting to {serial}")
    match = mops.expect([f"root@{serial}", "Giving up"], timeout=50)
    if match == 1:
        print("Unable to connect to monitor! Please confirm the monitor is online and the SSH tunnel is open.")
        exit(1)
    print("Connected to monitor!")
    mops.sendline("killall wattscoop")
    sleep(2)
    mops.sendline("sensectl stop wattscoop")
    print("Stopped wattscoop...")

    try:
        mops.expect("canceled", timeout=10)
    except TIMEOUT:
        print("Unable to stop wattscoop..")
        exit(1)
    mops.sendline("sample_adc low_speed")
    print("Running sample_adc test...")
    try:
        mops.expect(".wav",timeout=10)
    except TIMEOUT:
        print("Failed to generate .wav file on monitor!")
        exit(1)
    output_file = mops.before.decode("utf-8").split("\n")[-1].strip()
    file_name = output_file.split("/")[-1]
    print(f"Created file on monitor: {output_file}")
    mops.sendline("sensectl restart wattscoop")
    print("Restarted wattscoop on monitor...")
    sleep(3)
    mops.sendline("exit")
    sleep(2)

    print("Copying file to DS machine...")
    scp = pexpect.spawn(f"{INTERPRETER_PATH} /data/shared-support/code/monitorsw/src/tools/mops.py {serial} scp :{output_file}.wav /tmp/{file_name}-{serial}.wav")
    try:
        scp.expect("100%", timeout=30)
    except TIMEOUT:
        print("Failed to copy file from monitor to DS machine...")
        exit(1)
    print(f"File saved to: /tmp/{file_name}-{serial}.wav")


def main():
    args = parse_opts()
    run(args.serial)
main()
