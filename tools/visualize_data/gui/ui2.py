# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui2.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1006, 523)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.CHANNEL_2_VOLTAGE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_2_VOLTAGE.setObjectName("CHANNEL_2_VOLTAGE")
        self.CHANNEL_2_VOLTAGE.addItem("")
        self.CHANNEL_2_VOLTAGE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_2_VOLTAGE, 2, 2, 1, 1)
        self.CHANNEL_3_TYPE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_3_TYPE.setObjectName("CHANNEL_3_TYPE")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.CHANNEL_3_TYPE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_3_TYPE, 3, 6, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.tab)
        self.label_8.setObjectName("label_8")
        self.gridLayout.addWidget(self.label_8, 1, 1, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.tab)
        self.label_11.setObjectName("label_11")
        self.gridLayout.addWidget(self.label_11, 2, 1, 1, 1)
        self.label_13 = QtWidgets.QLabel(self.tab)
        self.label_13.setObjectName("label_13")
        self.gridLayout.addWidget(self.label_13, 2, 5, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.tab)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 1, 3, 1, 1)
        self.CHANNEL_0_VOLTAGE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_0_VOLTAGE.setObjectName("CHANNEL_0_VOLTAGE")
        self.CHANNEL_0_VOLTAGE.addItem("")
        self.CHANNEL_0_VOLTAGE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_0_VOLTAGE, 0, 2, 1, 1)
        self.label_19 = QtWidgets.QLabel(self.tab)
        self.label_19.setObjectName("label_19")
        self.gridLayout.addWidget(self.label_19, 4, 0, 1, 1)
        self.CHANNEL_1_TYPE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_1_TYPE.setObjectName("CHANNEL_1_TYPE")
        self.CHANNEL_1_TYPE.addItem("")
        self.CHANNEL_1_TYPE.addItem("")
        self.CHANNEL_1_TYPE.addItem("")
        self.CHANNEL_1_TYPE.addItem("")
        self.CHANNEL_1_TYPE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_1_TYPE, 1, 6, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.tab)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 0, 1, 1, 1)
        self.label_20 = QtWidgets.QLabel(self.tab)
        self.label_20.setObjectName("label_20")
        self.gridLayout.addWidget(self.label_20, 4, 4, 1, 1)
        self.CHANNEL_2_CT = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_2_CT.setObjectName("CHANNEL_2_CT")
        self.CHANNEL_2_CT.addItem("")
        self.CHANNEL_2_CT.addItem("")
        self.CHANNEL_2_CT.addItem("")
        self.CHANNEL_2_CT.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_2_CT, 2, 4, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.tab)
        self.label_7.setObjectName("label_7")
        self.gridLayout.addWidget(self.label_7, 2, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.tab)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.CHANNEL_3_VOLTAGE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_3_VOLTAGE.setObjectName("CHANNEL_3_VOLTAGE")
        self.CHANNEL_3_VOLTAGE.addItem("")
        self.CHANNEL_3_VOLTAGE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_3_VOLTAGE, 3, 2, 1, 1)
        self.label_6 = QtWidgets.QLabel(self.tab)
        self.label_6.setObjectName("label_6")
        self.gridLayout.addWidget(self.label_6, 0, 3, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.tab)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.CHANNEL_1_VOLTAGE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_1_VOLTAGE.setObjectName("CHANNEL_1_VOLTAGE")
        self.CHANNEL_1_VOLTAGE.addItem("")
        self.CHANNEL_1_VOLTAGE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_1_VOLTAGE, 1, 2, 1, 1)
        self.CHANNEL_0_CT = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_0_CT.setObjectName("CHANNEL_0_CT")
        self.CHANNEL_0_CT.addItem("")
        self.CHANNEL_0_CT.addItem("")
        self.CHANNEL_0_CT.addItem("")
        self.CHANNEL_0_CT.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_0_CT, 0, 4, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.tab)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 1, 5, 1, 1)
        self.label_17 = QtWidgets.QLabel(self.tab)
        self.label_17.setObjectName("label_17")
        self.gridLayout.addWidget(self.label_17, 0, 5, 1, 1)
        self.CHANNEL_1_CT = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_1_CT.setObjectName("CHANNEL_1_CT")
        self.CHANNEL_1_CT.addItem("")
        self.CHANNEL_1_CT.addItem("")
        self.CHANNEL_1_CT.addItem("")
        self.CHANNEL_1_CT.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_1_CT, 1, 4, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.tab)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 2, 3, 1, 1)
        self.label_15 = QtWidgets.QLabel(self.tab)
        self.label_15.setObjectName("label_15")
        self.gridLayout.addWidget(self.label_15, 3, 3, 1, 1)
        self.CHANNEL_3_CT = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_3_CT.setObjectName("CHANNEL_3_CT")
        self.CHANNEL_3_CT.addItem("")
        self.CHANNEL_3_CT.addItem("")
        self.CHANNEL_3_CT.addItem("")
        self.CHANNEL_3_CT.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_3_CT, 3, 4, 1, 1)
        self.EXPLICIT_CHANNEL_MAP = QtWidgets.QComboBox(self.tab)
        self.EXPLICIT_CHANNEL_MAP.setObjectName("EXPLICIT_CHANNEL_MAP")
        self.EXPLICIT_CHANNEL_MAP.addItem("")
        self.EXPLICIT_CHANNEL_MAP.addItem("")
        self.gridLayout.addWidget(self.EXPLICIT_CHANNEL_MAP, 4, 5, 1, 1)
        self.label_14 = QtWidgets.QLabel(self.tab)
        self.label_14.setObjectName("label_14")
        self.gridLayout.addWidget(self.label_14, 3, 1, 1, 1)
        self.CHANNEL_2_TYPE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_2_TYPE.setObjectName("CHANNEL_2_TYPE")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.CHANNEL_2_TYPE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_2_TYPE, 2, 6, 1, 1)
        self.CHANNEL_0_TYPE = QtWidgets.QComboBox(self.tab)
        self.CHANNEL_0_TYPE.setObjectName("CHANNEL_0_TYPE")
        self.CHANNEL_0_TYPE.addItem("")
        self.CHANNEL_0_TYPE.addItem("")
        self.CHANNEL_0_TYPE.addItem("")
        self.CHANNEL_0_TYPE.addItem("")
        self.CHANNEL_0_TYPE.addItem("")
        self.gridLayout.addWidget(self.CHANNEL_0_TYPE, 0, 6, 1, 1)
        self.AUX_PORT_MODE = QtWidgets.QComboBox(self.tab)
        self.AUX_PORT_MODE.setObjectName("AUX_PORT_MODE")
        self.AUX_PORT_MODE.addItem("")
        self.AUX_PORT_MODE.addItem("")
        self.AUX_PORT_MODE.addItem("")
        self.AUX_PORT_MODE.addItem("")
        self.AUX_PORT_MODE.addItem("")
        self.gridLayout.addWidget(self.AUX_PORT_MODE, 4, 1, 1, 1)
        self.label_16 = QtWidgets.QLabel(self.tab)
        self.label_16.setObjectName("label_16")
        self.gridLayout.addWidget(self.label_16, 3, 5, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.tab)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.label_18 = QtWidgets.QLabel(self.tab)
        self.label_18.setObjectName("label_18")
        self.gridLayout.addWidget(self.label_18, 5, 0, 1, 1)
        self.SOLAR_CONNECTED_TO_BREAKERS = QtWidgets.QComboBox(self.tab)
        self.SOLAR_CONNECTED_TO_BREAKERS.setObjectName(
            "SOLAR_CONNECTED_TO_BREAKERS")
        self.SOLAR_CONNECTED_TO_BREAKERS.addItem("")
        self.SOLAR_CONNECTED_TO_BREAKERS.addItem("")
        self.gridLayout.addWidget(self.SOLAR_CONNECTED_TO_BREAKERS, 5, 1, 1, 1)
        self.updateDCM_config = QtWidgets.QPushButton(self.tab)
        self.updateDCM_config.setObjectName("updateDCM_config")
        self.gridLayout.addWidget(self.updateDCM_config, 5, 6, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.tab_2)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.ConfigPage_Label_MainCTPolarity = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_MainCTPolarity.setObjectName(
            "ConfigPage_Label_MainCTPolarity")
        self.horizontalLayout_8.addWidget(self.ConfigPage_Label_MainCTPolarity)
        self.main_polarity = QtWidgets.QComboBox(self.tab_2)
        self.main_polarity.setObjectName("main_polarity")
        self.main_polarity.addItem("")
        self.main_polarity.addItem("")
        self.main_polarity.addItem("")
        self.main_polarity.addItem("")
        self.horizontalLayout_8.addWidget(self.main_polarity)
        self.gridLayout_3.addLayout(self.horizontalLayout_8, 0, 0, 1, 1)
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.ConfigPage_Label_UsingSolar = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_UsingSolar.setObjectName(
            "ConfigPage_Label_UsingSolar")
        self.horizontalLayout_13.addWidget(self.ConfigPage_Label_UsingSolar)
        self.aux_port_mode = QtWidgets.QComboBox(self.tab_2)
        self.aux_port_mode.setObjectName("aux_port_mode")
        self.aux_port_mode.addItem("")
        self.aux_port_mode.addItem("")
        self.aux_port_mode.addItem("")
        self.aux_port_mode.addItem("")
        self.aux_port_mode.addItem("")
        self.horizontalLayout_13.addWidget(self.aux_port_mode)
        self.gridLayout_3.addLayout(self.horizontalLayout_13, 4, 0, 1, 1)
        self.horizontalLayout_21 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_21.setObjectName("horizontalLayout_21")
        self.ConfigPage_Label_NumChannels = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_NumChannels.setObjectName(
            "ConfigPage_Label_NumChannels")
        self.horizontalLayout_21.addWidget(self.ConfigPage_Label_NumChannels)
        self.num_main_channels = QtWidgets.QLineEdit(self.tab_2)
        self.num_main_channels.setObjectName("num_main_channels")
        self.horizontalLayout_21.addWidget(self.num_main_channels)
        self.gridLayout_3.addLayout(self.horizontalLayout_21, 7, 0, 1, 1)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.ConfigPage_Label_SolarCTPolarity = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_SolarCTPolarity.setObjectName(
            "ConfigPage_Label_SolarCTPolarity")
        self.horizontalLayout_10.addWidget(
            self.ConfigPage_Label_SolarCTPolarity)
        self.solar_polarity = QtWidgets.QComboBox(self.tab_2)
        self.solar_polarity.setObjectName("solar_polarity")
        self.solar_polarity.addItem("")
        self.solar_polarity.addItem("")
        self.solar_polarity.addItem("")
        self.solar_polarity.addItem("")
        self.horizontalLayout_10.addWidget(self.solar_polarity)
        self.gridLayout_3.addLayout(self.horizontalLayout_10, 1, 0, 1, 1)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.ConfigPage_Label_SwapMains = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_SwapMains.setObjectName(
            "ConfigPage_Label_SwapMains")
        self.horizontalLayout_11.addWidget(self.ConfigPage_Label_SwapMains)
        self.swap_mains = QtWidgets.QComboBox(self.tab_2)
        self.swap_mains.setObjectName("swap_mains")
        self.swap_mains.addItem("")
        self.swap_mains.addItem("")
        self.horizontalLayout_11.addWidget(self.swap_mains)
        self.gridLayout_3.addLayout(self.horizontalLayout_11, 2, 0, 1, 1)
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.ConfigPage_Label_SolarConnectedToBreakers = QtWidgets.QLabel(
            self.tab_2)
        self.ConfigPage_Label_SolarConnectedToBreakers.setObjectName(
            "ConfigPage_Label_SolarConnectedToBreakers")
        self.horizontalLayout_15.addWidget(
            self.ConfigPage_Label_SolarConnectedToBreakers)
        self.sctb = QtWidgets.QComboBox(self.tab_2)
        self.sctb.setObjectName("sctb")
        self.sctb.addItem("")
        self.sctb.addItem("")
        self.horizontalLayout_15.addWidget(self.sctb)
        self.gridLayout_3.addLayout(self.horizontalLayout_15, 5, 0, 1, 1)
        self.horizontalLayout_22 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_22.setObjectName("horizontalLayout_22")
        self.horizontalLayout_25 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_25.setObjectName("horizontalLayout_25")
        self.ConfigPage_Label_SolarMainsSwapped = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_SolarMainsSwapped.setObjectName(
            "ConfigPage_Label_SolarMainsSwapped")
        self.horizontalLayout_25.addWidget(
            self.ConfigPage_Label_SolarMainsSwapped)
        self.solar_mains_swapped = QtWidgets.QComboBox(self.tab_2)
        self.solar_mains_swapped.setObjectName("solar_mains_swapped")
        self.solar_mains_swapped.addItem("")
        self.solar_mains_swapped.addItem("")
        self.horizontalLayout_25.addWidget(self.solar_mains_swapped)
        self.horizontalLayout_22.addLayout(self.horizontalLayout_25)
        self.gridLayout_3.addLayout(self.horizontalLayout_22, 8, 0, 1, 1)
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.ConfigPage_Label_SwapSolar = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_SwapSolar.setObjectName(
            "ConfigPage_Label_SwapSolar")
        self.horizontalLayout_12.addWidget(self.ConfigPage_Label_SwapSolar)
        self.swap_solar = QtWidgets.QComboBox(self.tab_2)
        self.swap_solar.setObjectName("swap_solar")
        self.swap_solar.addItem("")
        self.swap_solar.addItem("")
        self.horizontalLayout_12.addWidget(self.swap_solar)
        self.gridLayout_3.addLayout(self.horizontalLayout_12, 3, 0, 1, 1)
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.ConfigPage_Label_FrameRate = QtWidgets.QLabel(self.tab_2)
        self.ConfigPage_Label_FrameRate.setObjectName(
            "ConfigPage_Label_FrameRate")
        self.horizontalLayout_16.addWidget(self.ConfigPage_Label_FrameRate)
        self.frame_rate = QtWidgets.QLineEdit(self.tab_2)
        self.frame_rate.setText("")
        self.frame_rate.setObjectName("frame_rate")
        self.horizontalLayout_16.addWidget(self.frame_rate)
        self.gridLayout_3.addLayout(self.horizontalLayout_16, 6, 0, 1, 1)
        self.ConfigPage_PushButton_PushChanges = QtWidgets.QPushButton(
            self.tab_2)
        self.ConfigPage_PushButton_PushChanges.setObjectName(
            "ConfigPage_PushButton_PushChanges")
        self.gridLayout_3.addWidget(
            self.ConfigPage_PushButton_PushChanges, 9, 0, 1, 1)
        self.horizontalLayout_2.addLayout(self.gridLayout_3)
        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.verticalLayout_6.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_27 = QtWidgets.QLabel(self.tab_3)
        self.label_27.setObjectName("label_27")
        self.horizontalLayout_4.addWidget(self.label_27)
        self.setup_logs_selected = QtWidgets.QRadioButton(self.tab_3)
        self.setup_logs_selected.setChecked(True)
        self.setup_logs_selected.setObjectName("setup_logs_selected")
        self.horizontalLayout_4.addWidget(self.setup_logs_selected)
        self.network_test_selected = QtWidgets.QRadioButton(self.tab_3)
        self.network_test_selected.setObjectName("network_test_selected")
        self.horizontalLayout_4.addWidget(self.network_test_selected)
        self.verticalLayout_6.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_24 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_24.setObjectName("horizontalLayout_24")
        self.label_28 = QtWidgets.QLabel(self.tab_3)
        self.label_28.setObjectName("label_28")
        self.horizontalLayout_24.addWidget(self.label_28)
        self.result_type = QtWidgets.QComboBox(self.tab_3)
        self.result_type.setObjectName("result_type")
        self.result_type.addItem("")
        self.result_type.addItem("")
        self.result_type.addItem("")
        self.horizontalLayout_24.addWidget(self.result_type)
        self.verticalLayout_6.addLayout(self.horizontalLayout_24)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_21 = QtWidgets.QLabel(self.tab_3)
        self.label_21.setObjectName("label_21")
        self.horizontalLayout_7.addWidget(self.label_21)
        self.logs_monitor_id = QtWidgets.QLineEdit(self.tab_3)
        self.logs_monitor_id.setObjectName("logs_monitor_id")
        self.horizontalLayout_7.addWidget(self.logs_monitor_id)
        self.load_setup_logs = QtWidgets.QPushButton(self.tab_3)
        self.load_setup_logs.setObjectName("load_setup_logs")
        self.horizontalLayout_7.addWidget(self.load_setup_logs)
        self.verticalLayout_6.addLayout(self.horizontalLayout_7)
        self.gridLayout_4.addLayout(self.verticalLayout_6, 0, 0, 1, 1)
        self.verticalLayout_9 = QtWidgets.QVBoxLayout()
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.label_25 = QtWidgets.QLabel(self.tab_3)
        self.label_25.setObjectName("label_25")
        self.verticalLayout_9.addWidget(
            self.label_25, 0, QtCore.Qt.AlignHCenter)
        self.client_log_output = QtWidgets.QTextBrowser(self.tab_3)
        self.client_log_output.setObjectName("client_log_output")
        self.verticalLayout_9.addWidget(self.client_log_output)
        self.gridLayout_4.addLayout(self.verticalLayout_9, 0, 1, 2, 1)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_24 = QtWidgets.QLabel(self.tab_3)
        self.label_24.setObjectName("label_24")
        self.verticalLayout_7.addWidget(
            self.label_24, 0, QtCore.Qt.AlignHCenter)
        self.log_dates_list = QtWidgets.QListWidget(self.tab_3)
        self.log_dates_list.setObjectName("log_dates_list")
        self.verticalLayout_7.addWidget(self.log_dates_list)
        self.load_selected_log = QtWidgets.QPushButton(self.tab_3)
        self.load_selected_log.setObjectName("load_selected_log")
        self.verticalLayout_7.addWidget(self.load_selected_log)
        self.gridLayout_4.addLayout(self.verticalLayout_7, 1, 0, 1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_4 = QtWidgets.QWidget()
        self.tab_4.setObjectName("tab_4")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.tab_4)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.formLayout_2 = QtWidgets.QFormLayout()
        self.formLayout_2.setObjectName("formLayout_2")
        self.label_22 = QtWidgets.QLabel(self.tab_4)
        self.label_22.setObjectName("label_22")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.LabelRole, self.label_22)
        self.email = QtWidgets.QLineEdit(self.tab_4)
        self.email.setObjectName("email")
        self.formLayout_2.setWidget(
            0, QtWidgets.QFormLayout.FieldRole, self.email)
        self.label_23 = QtWidgets.QLabel(self.tab_4)
        self.label_23.setObjectName("label_23")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.label_23)
        self.password = QtWidgets.QLineEdit(self.tab_4)
        self.password.setText("")
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password.setObjectName("password")
        self.formLayout_2.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.password)
        self.verticalLayout_4.addLayout(self.formLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.authenticate = QtWidgets.QPushButton(self.tab_4)
        self.authenticate.setObjectName("authenticate")
        self.verticalLayout_3.addWidget(self.authenticate)
        self.check_token = QtWidgets.QPushButton(self.tab_4)
        self.check_token.setObjectName("check_token")
        self.verticalLayout_3.addWidget(self.check_token)
        self.remove_token = QtWidgets.QPushButton(self.tab_4)
        self.remove_token.setObjectName("remove_token")
        self.verticalLayout_3.addWidget(self.remove_token)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)
        self.tabWidget.addTab(self.tab_4, "")
        self.tab_5 = QtWidgets.QWidget()
        self.tab_5.setObjectName("tab_5")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.tab_5)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.LoggingPage_Label_Console = QtWidgets.QLabel(self.tab_5)
        self.LoggingPage_Label_Console.setObjectName(
            "LoggingPage_Label_Console")
        self.verticalLayout_5.addWidget(self.LoggingPage_Label_Console)
        self.LoggingPage_TextEdit_Console = QtWidgets.QTextEdit(self.tab_5)
        self.LoggingPage_TextEdit_Console.setReadOnly(True)
        self.LoggingPage_TextEdit_Console.setObjectName(
            "LoggingPage_TextEdit_Console")
        self.verticalLayout_5.addWidget(self.LoggingPage_TextEdit_Console)
        self.LoggingPage_PushButton_Clear = QtWidgets.QPushButton(self.tab_5)
        self.LoggingPage_PushButton_Clear.setObjectName(
            "LoggingPage_PushButton_Clear")
        self.verticalLayout_5.addWidget(self.LoggingPage_PushButton_Clear)
        self.tabWidget.addTab(self.tab_5, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.monitor_id_input = QtWidgets.QLineEdit(self.centralwidget)
        self.monitor_id_input.setObjectName("monitor_id_input")
        self.horizontalLayout.addWidget(self.monitor_id_input)
        self.loadHouse = QtWidgets.QPushButton(self.centralwidget)
        self.loadHouse.setObjectName("loadHouse")
        self.horizontalLayout.addWidget(self.loadHouse)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        # MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        # MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.CHANNEL_2_VOLTAGE.setItemText(0, _translate("MainWindow", "V0"))
        self.CHANNEL_2_VOLTAGE.setItemText(1, _translate("MainWindow", "V1"))
        self.CHANNEL_3_TYPE.setItemText(0, _translate("MainWindow", "NONE"))
        self.CHANNEL_3_TYPE.setItemText(1, _translate("MainWindow", "MAINS"))
        self.CHANNEL_3_TYPE.setItemText(2, _translate("MainWindow", "SOLAR"))
        self.CHANNEL_3_TYPE.setItemText(
            3, _translate("MainWindow", "SPLIT_SERVICE"))
        self.CHANNEL_3_TYPE.setItemText(
            4, _translate("MainWindow", "GENERATOR"))
        self.CHANNEL_3_TYPE.setItemText(
            5, _translate("MainWindow", "DEDICATED_120V"))
        self.CHANNEL_3_TYPE.setItemText(
            6, _translate("MainWindow", "DEDICATED_240V"))
        self.CHANNEL_3_TYPE.setItemText(
            7, _translate("MainWindow", "DEDICATED_120/240V"))
        self.CHANNEL_3_TYPE.setItemText(
            8, _translate("MainWindow", "DEDICATED_D240V"))
        self.CHANNEL_3_TYPE.setItemText(
            9, _translate("MainWindow", "SPLIT_SERVICE"))
        self.CHANNEL_3_TYPE.setItemText(
            10, _translate("MainWindow", "FAKE_3_PHASE"))
        self.label_8.setText(_translate("MainWindow", "Voltage:"))
        self.label_11.setText(_translate("MainWindow", "Voltage:"))
        self.label_13.setText(_translate("MainWindow", "Type:"))
        self.label_9.setText(_translate("MainWindow", "CT:"))
        self.CHANNEL_0_VOLTAGE.setItemText(0, _translate("MainWindow", "V0"))
        self.CHANNEL_0_VOLTAGE.setItemText(1, _translate("MainWindow", "V1"))
        self.label_19.setText(_translate("MainWindow", "AUX_PORT_MODE:"))
        self.CHANNEL_1_TYPE.setItemText(0, _translate("MainWindow", "MAINS"))
        self.CHANNEL_1_TYPE.setItemText(1, _translate("MainWindow", "SOLAR"))
        self.CHANNEL_1_TYPE.setItemText(
            2, _translate("MainWindow", "SPLIT_SERVICE"))
        self.CHANNEL_1_TYPE.setItemText(
            3, _translate("MainWindow", "GENERATOR"))
        self.CHANNEL_1_TYPE.setItemText(4, _translate("MainWindow", "NONE"))
        self.label_5.setText(_translate("MainWindow", "Voltage:"))
        self.label_20.setText(_translate(
            "MainWindow", "EXPLICIT_CHANNEL_MAP:"))
        self.CHANNEL_2_CT.setItemText(0, _translate("MainWindow", "CT2"))
        self.CHANNEL_2_CT.setItemText(1, _translate("MainWindow", "-CT2"))
        self.CHANNEL_2_CT.setItemText(2, _translate("MainWindow", "CT3"))
        self.CHANNEL_2_CT.setItemText(3, _translate("MainWindow", "-CT3"))
        self.label_7.setText(_translate("MainWindow", "CHANNEL_2:"))
        self.label_4.setText(_translate("MainWindow", "CHANNEL_3:"))
        self.CHANNEL_3_VOLTAGE.setItemText(0, _translate("MainWindow", "V0"))
        self.CHANNEL_3_VOLTAGE.setItemText(1, _translate("MainWindow", "V1"))
        self.label_6.setText(_translate("MainWindow", "CT:"))
        self.label_2.setText(_translate("MainWindow", "CHANNEL_1:"))
        self.CHANNEL_1_VOLTAGE.setItemText(0, _translate("MainWindow", "V0"))
        self.CHANNEL_1_VOLTAGE.setItemText(1, _translate("MainWindow", "V1"))
        self.CHANNEL_0_CT.setItemText(0, _translate("MainWindow", "CT0"))
        self.CHANNEL_0_CT.setItemText(1, _translate("MainWindow", "-CT0"))
        self.CHANNEL_0_CT.setItemText(2, _translate("MainWindow", "CT1"))
        self.CHANNEL_0_CT.setItemText(3, _translate("MainWindow", "-CT1"))
        self.label_10.setText(_translate("MainWindow", "Type:"))
        self.label_17.setText(_translate("MainWindow", "Type:"))
        self.CHANNEL_1_CT.setItemText(0, _translate("MainWindow", "CT1"))
        self.CHANNEL_1_CT.setItemText(1, _translate("MainWindow", "-CT1"))
        self.CHANNEL_1_CT.setItemText(2, _translate("MainWindow", "CT0"))
        self.CHANNEL_1_CT.setItemText(3, _translate("MainWindow", "-CT0"))
        self.label_12.setText(_translate("MainWindow", "CT:"))
        self.label_15.setText(_translate("MainWindow", "CT:"))
        self.CHANNEL_3_CT.setItemText(0, _translate("MainWindow", "CT3"))
        self.CHANNEL_3_CT.setItemText(1, _translate("MainWindow", "-CT3"))
        self.CHANNEL_3_CT.setItemText(2, _translate("MainWindow", "CT2"))
        self.CHANNEL_3_CT.setItemText(3, _translate("MainWindow", "-CT2"))
        self.EXPLICIT_CHANNEL_MAP.setItemText(0, _translate("MainWindow", "0"))
        self.EXPLICIT_CHANNEL_MAP.setItemText(1, _translate("MainWindow", "1"))
        self.label_14.setText(_translate("MainWindow", "Voltage:"))
        self.CHANNEL_2_TYPE.setItemText(0, _translate("MainWindow", "NONE"))
        self.CHANNEL_2_TYPE.setItemText(1, _translate("MainWindow", "MAINS"))
        self.CHANNEL_2_TYPE.setItemText(2, _translate("MainWindow", "SOLAR"))
        self.CHANNEL_2_TYPE.setItemText(
            3, _translate("MainWindow", "SPLIT_SERVICE"))
        self.CHANNEL_2_TYPE.setItemText(
            4, _translate("MainWindow", "GENERATOR"))
        self.CHANNEL_2_TYPE.setItemText(
            5, _translate("MainWindow", "DEDICATED_120V"))
        self.CHANNEL_2_TYPE.setItemText(
            6, _translate("MainWindow", "DEDICATED_240V"))
        self.CHANNEL_2_TYPE.setItemText(
            7, _translate("MainWindow", "DEDICATED_120/240V"))
        self.CHANNEL_2_TYPE.setItemText(
            8, _translate("MainWindow", "DEDICATED_D240V"))
        self.CHANNEL_2_TYPE.setItemText(
            9, _translate("MainWindow", "FAKE_3_PHASE"))
        self.CHANNEL_0_TYPE.setItemText(0, _translate("MainWindow", "MAINS"))
        self.CHANNEL_0_TYPE.setItemText(1, _translate("MainWindow", "SOLAR"))
        self.CHANNEL_0_TYPE.setItemText(
            2, _translate("MainWindow", "SPLIT_SERVICE"))
        self.CHANNEL_0_TYPE.setItemText(
            3, _translate("MainWindow", "GENERATOR"))
        self.CHANNEL_0_TYPE.setItemText(4, _translate("MainWindow", "NONE"))
        self.AUX_PORT_MODE.setItemText(0, _translate("MainWindow", "ignore"))
        self.AUX_PORT_MODE.setItemText(1, _translate("MainWindow", "solar"))
        self.AUX_PORT_MODE.setItemText(
            2, _translate("MainWindow", "split_service"))
        self.AUX_PORT_MODE.setItemText(
            3, _translate("MainWindow", "generator"))
        self.AUX_PORT_MODE.setItemText(
            4, _translate("MainWindow", "dedicated_circuit"))
        self.label_16.setText(_translate("MainWindow", "Type:"))
        self.label_3.setText(_translate("MainWindow", "CHANNEL_0:"))
        self.label_18.setText(_translate("MainWindow", "SCTB:"))
        self.SOLAR_CONNECTED_TO_BREAKERS.setItemText(
            0, _translate("MainWindow", "0"))
        self.SOLAR_CONNECTED_TO_BREAKERS.setItemText(
            1, _translate("MainWindow", "1"))
        self.updateDCM_config.setText(_translate("MainWindow", "Push Updates"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(
            self.tab), _translate("MainWindow", "New Syntax"))
        self.ConfigPage_Label_MainCTPolarity.setText(
            _translate("MainWindow", "Main CTs Polarity:"))
        self.main_polarity.setItemText(0, _translate("MainWindow", "0,0"))
        self.main_polarity.setItemText(1, _translate("MainWindow", "0,1"))
        self.main_polarity.setItemText(2, _translate("MainWindow", "1,0"))
        self.main_polarity.setItemText(3, _translate("MainWindow", "1,1"))
        self.ConfigPage_Label_UsingSolar.setText(
            _translate("MainWindow", "Aux Port Mode:"))
        self.aux_port_mode.setItemText(0, _translate("MainWindow", "ignore"))
        self.aux_port_mode.setItemText(1, _translate("MainWindow", "solar"))
        self.aux_port_mode.setItemText(
            2, _translate("MainWindow", "generator"))
        self.aux_port_mode.setItemText(
            3, _translate("MainWindow", "split_service"))
        self.aux_port_mode.setItemText(
            4, _translate("MainWindow", "dedicated_circuit"))
        self.ConfigPage_Label_NumChannels.setText(
            _translate("MainWindow", "Number of main channels:"))
        self.num_main_channels.setPlaceholderText(
            _translate("MainWindow", "2"))
        self.ConfigPage_Label_SolarCTPolarity.setText(
            _translate("MainWindow", "Solar CTs Polarity:"))
        self.solar_polarity.setItemText(0, _translate("MainWindow", "0,0"))
        self.solar_polarity.setItemText(1, _translate("MainWindow", "0,1"))
        self.solar_polarity.setItemText(2, _translate("MainWindow", "1,0"))
        self.solar_polarity.setItemText(3, _translate("MainWindow", "1,1"))
        self.ConfigPage_Label_SwapMains.setText(
            _translate("MainWindow", "Swap Mains:"))
        self.swap_mains.setItemText(0, _translate("MainWindow", "0"))
        self.swap_mains.setItemText(1, _translate("MainWindow", "1"))
        self.ConfigPage_Label_SolarConnectedToBreakers.setText(
            _translate("MainWindow", "SCTB"))
        self.sctb.setItemText(0, _translate("MainWindow", "0"))
        self.sctb.setItemText(1, _translate("MainWindow", "1"))
        self.ConfigPage_Label_SolarMainsSwapped.setText(
            _translate("MainWindow", "Solar Mains Swapped:"))
        self.solar_mains_swapped.setItemText(0, _translate("MainWindow", "0"))
        self.solar_mains_swapped.setItemText(1, _translate("MainWindow", "1"))
        self.ConfigPage_Label_SwapSolar.setText(
            _translate("MainWindow", "Swap Solar:"))
        self.swap_solar.setItemText(0, _translate("MainWindow", "0"))
        self.swap_solar.setItemText(1, _translate("MainWindow", "1"))
        self.ConfigPage_Label_FrameRate.setText(
            _translate("MainWindow", "Frame Rate:"))
        self.frame_rate.setPlaceholderText(_translate("MainWindow", "60"))
        self.ConfigPage_PushButton_PushChanges.setText(
            _translate("MainWindow", "Update"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(
            self.tab_2), _translate("MainWindow", "Old Syntax"))
        self.label_27.setText(_translate("MainWindow", "Type:"))
        self.setup_logs_selected.setText(
            _translate("MainWindow", "Setup Log(s)"))
        self.network_test_selected.setText(
            _translate("MainWindow", "Network Test(s)"))
        self.label_28.setText(_translate("MainWindow", "Test Result:"))
        self.result_type.setItemText(0, _translate("MainWindow", "All"))
        self.result_type.setItemText(1, _translate("MainWindow", "Failure"))
        self.result_type.setItemText(2, _translate("MainWindow", "Success"))
        self.label_21.setText(_translate("MainWindow", "Monitor ID:"))
        self.load_setup_logs.setText(_translate("MainWindow", "Load"))
        self.label_25.setText(_translate("MainWindow", "Output:"))
        self.label_24.setText(_translate("MainWindow", "Avaliable Logs:"))
        self.load_selected_log.setText(
            _translate("MainWindow", "Load Selected"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(
            self.tab_3), _translate("MainWindow", "Logs"))
        self.label_22.setText(_translate("MainWindow", "Admin Console Email:"))
        self.label_23.setText(_translate(
            "MainWindow", "Admin Console Password:"))
        self.authenticate.setText(_translate("MainWindow", "Authenticate"))
        self.check_token.setText(_translate(
            "MainWindow", "Check Saved Authentication Token Validity"))
        self.remove_token.setText(_translate(
            "MainWindow", "Remove Saved Authentication Token"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(
            self.tab_4), _translate("MainWindow", "Authenticate"))
        self.LoggingPage_Label_Console.setText(
            _translate("MainWindow", "Console:"))
        self.LoggingPage_PushButton_Clear.setText(
            _translate("MainWindow", "Clear"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(
            self.tab_5), _translate("MainWindow", "Console"))
        self.label.setText(_translate("MainWindow", "Monitor ID:"))
        self.loadHouse.setText(_translate("MainWindow", "Load"))
