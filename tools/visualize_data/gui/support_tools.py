from os import name
from typing import Collection
from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject, pyqtSignal
from pprint import pprint
import sys
import ui
from time import sleep
from Sense.Api import Auth, Monitor
import subprocess
from requests import Response
from datetime import datetime
from os.path import expanduser, exists
import logging
from json import dumps
from dateutil import parser

logging.basicConfig(filename='log.log', level=logging.INFO)


class MainWindow(QMainWindow, ui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        error_message = QMessageBox()
        self.error_message = error_message

        # self.CHANNEL_0_CT, self.CHANNEL_0_TYPE, self.CHANNEL_0_VOLTAGE, self.CHANNEL_1_CT, self.CHANNEL_1_TYPE, self.CHANNEL_1_VOLTAGE, self.CHANNEL_2_CT, self.CHANNEL_2_TYPE, self.CHANNEL_3_CT, self.CHANNEL_3_TYPE, self.CHANNEL_3_VOLTAGE,
        buttons = [self.loadHouse,
                   self.ConfigPage_PushButton_PushChanges, self.authenticate, self.check_token, self.remove_token, self.load_setup_logs, self.load_selected_log, self.updateTimeZone_btn, self.pushSignalCheck_btn,
                   self.closeChannelMapRequest_btn, self.updateDCM_config]
        # Disable wattsurfer tools until working
        # self.tabWidget.setTabEnabled(3, False)
        for button in buttons:
            if button.clicked.connect(self.PageListener):
                pass

    def PageListener(self):
        monitor_field = self.monitor_id_input.text()
        self.monitor_id = self.monitor_id_input.text()
        listener = self.sender()
        self.matcher = listener.objectName()
        if self.matcher == "loadHouse":
            try:
                self.create_monitor(self.monitor_id)
            except ModuleNotFoundError as e:
                print(e)
                self.error_message.setText(
                    f"h{monitor_field} does not exist!")
                self.error_message.show()
            if self.usesDCM():
                self.tabWidget.setTabEnabled(0, self.usesDCM())
                self.tabWidget.setTabEnabled(1, False)
                self.load_house()

            if not self.usesDCM():
                self.tabWidget.setTabEnabled(1, True)
                self.tabWidget.setTabEnabled(0, False)
                self.load_old_channel_map()

        if self.matcher == "updateDCM_config":
            self.update_strings()
            self.update_channelmap()
        if self.matcher == "ConfigPage_PushButton_PushChanges":
            self.update_old_channel_map()
            sleep(1)
            self.load_old_channel_map()
        if self.matcher == "authenticate":
            self.authenticate_user()
        if self.matcher == "updateTimeZone_btn":
            self.update_timezone()
        if self.matcher == "pushSignalCheck_btn":
            self.push_signal_check()
        if self.matcher == "closeChannelMapRequest_btn":
            self.close_channel_map()
        if self.matcher == "check_token":
            self.test_auth()
        if self.matcher == "remove_token":
            self.remove_auth_token()
        if self.matcher == "load_setup_logs":
            try:
                if self.setup_logs_selected.isChecked():  # Setup Logs
                    self.result_date = self.get_setup_logs(
                        self.result_type.currentText())
                elif self.network_test_selected.isChecked():  # Network Tests
                    self.result_date = self.get_network_tests(
                        self.result_type.currentText())
            except TypeError as e:
                self.error_message.setText("Invalid Monitor ID.")
                self.error_message.show()
                self.__ui_logger(
                    "Invalid monitor id, threw TypeError: {}".format(e))

        if self.matcher == "load_selected_log":
            try:
                if self.setup_logs_selected.isChecked():  # Setup Logs
                    self.select_setup_log(self.result_date, mode="logs")
                elif self.network_test_selected.isChecked():
                    # self.select_setup_log(self.result_date, mode="network")
                    self.select_setup_log(self.result_date, mode="network")
            except IndexError as e:
                self.error_message.setText("No log selected!")
                self.error_message.show()
                self.__ui_logger(
                    "No log selected by user, failed for IndexError, val: {}".format(e))

    def authenticate_user(self):
        try:
            token = Auth(require_input=False, email=self.email.text(),
                         password=self.password.text()).token
            if len(token) > 0:
                self.__ui_logger("Stored token: {}".format(token))
                self.error_message.setText("Generated Authentication Token")
                self.error_message.show()
            else:
                self.__ui_logger("Couldn't generate auth token")
                self.error_message.setText(
                    "Could not generate token, check credentials")
                self.error_message.show()
        except KeyError:
            self.error_message.setText(
                "Could not successfully authenticate you.\nEnsure email/password combo are correct.")
            self.error_message.show()
            self.__ui_logger("Validation failed")

    def remove_auth_token(self):
        try:
            rm = Auth(require_input=False).remove_auth()
            if rm:
                self.__ui_logger("Removed auth token")
                self.error_message.setText(
                    "Removed Authentication Token.\nPlease re-run Authenicate to generate")
                self.error_message.show()
            else:
                self.__ui_logger("Failed to remove auth token")
                self.error_message.setText("Failed to remove authentication.")
                self.error_message.show()
        except KeyError as e:
            self.__ui_logger("Failed to remove token: {}".format(e))
            self.error_message.setText("No token saved, re-run authentication")
            self.error_message.show()

    def test_auth(self):
        try:
            token_valid = Auth(require_input=False).test_auth()
            if token_valid:
                self.error_message.setText("Token is valid.")
                self.__ui_logger("Token validity: {}".format(token_valid))
                self.error_message.show()
            else:
                self.error_message.setText("Token invalid! Re-authenticate")
                self.__ui_logger("Invalid auth token: {}".format(token_valid))
                self.error_message.show()
        except KeyError as e:
            self.__ui_logger("Failed to check token: {}".format(e))
            self.error_message.setText("No token saved, re-run authentication")
            self.error_message.show()

    def usesDCM(self):
        if "EXPLICIT_CHANNEL_MAP" in self.channelMap.keys():
            # if 1 it means we are using explicit cm
            if self.channelMap["EXPLICIT_CHANNEL_MAP"] == "1":
                return True
            else:
                return False
        else:
            return False

    def update_timezone(self):
        choices = {"EST": "America/New_York",
                   "CST": "America/Chicago",
                   "MST": "America/Denver",
                   "PST": "America/Los_Angeles",
                   "ADT": "America/Halifax"}
        DST_CHOICES = {"EST": "America/Montserrat",
                       "MST": "America/Creston",
                       "PST": "America/Los_Angeles",
                       "CST": "America/Chicago",
                       "ADT": "America/Guyana"}
        if self.no_dst.isChecked():
            choices = DST_CHOICES
        new_time_zone = choices[self.timezone_combobox.currentText()]
        response = self.monitor.update_timezone(new_time_zone)
        self.__ui_logger(
            f"Requested to update monitor id: {self.monitor.monitor_id} timezonet to: {new_time_zone}. DST checkbox value: {self.no_dst.isChecked()}")
        if response <= 203:
            self.__ui_logger(
                f"Successfully updated timezone, response code: {response}")

            self.error_message.setText(
                f"Successfully updated h{self.monitor.monitor_id} to: {new_time_zone}.")
            self.error_message.show()
        else:
            self.__ui_logger(
                f"Failed to updated monitor: {self.monitor.monitor_id}! Response status code: {response}", ERROR=True)
            self.error_message.setText(
                f"Failed to update timezone for: {self.monitor.monitor_id}, status code: {response}")
            self.error_message.show()

    def push_signal_check(self):
        r = self.monitor.pass_signal_check()
        if r <= 201:
            self.__ui_logger(
                f"Monitor: {self.monitor.monitor_id}, pushed through Signal Check, response: {r}")
            self.error_message.setText(
                f"Successfully pushed h{self.monitor.monitor_id} through signal check.")
            self.error_message.show()
        else:
            self.__ui_logger(
                f"Monitor: {self.monitor.monitor_id}, FAILED to push through Signal Check, response: {r}",  ERROR=True)
            self.error_message.setText(
                f"Failed to pushed h{self.monitor.monitor_id} through signal check.")
            self.error_message.show()
        self.create_monitor(self.monitor.monitor_id)  # reload

    def create_monitor(self, monitor_id):
        self.monitor = Monitor(monitor_id, Auth().token)
        self.closeChannelMapRequest_btn.setEnabled(True)
        self.loaded_house = self.monitor.monitor_id
        try:
            self.channelMap = self.monitor.get_channelmap()
            if type(self.channelMap) == Response:
                self.channelMap = self.channelMap.json()
        except KeyError as e:
            self.channelMap = {
                "MAIN_CT_POLARITY_BACKWARDS": "0,0",
                "SOLAR_CT_POLARITY_BACKWARDS": "0,0",
                "AUX_PORT_MODE": "ignore",
                "LEFT_RIGHT_SOLAR_SWAPPED": "0",
                "LEFT_RIGHT_MAINS_SWAPPED": "0",
                "SOLAR_CONNECTED_TO_BREAKERS": "0",
                "SOLAR_MAINS_SWAPPED": "0",
                "NUM_MAINS_CHANNELS": "2",
                "FRAME_RATE": "60"}
            self.__ui_logger(
                f"Failed to get r['channel_map'] for {self.monitor.monitor_id}, loading default..")
            self.__ui_logger(
                f"------------------------------\n{e}\n------------------------------\n")
        self.loaded_house_label.setText(
            "h{}".format(str(self.monitor.monitor_id)))
        self.monitor_data = self.monitor.get_monitor_status()
        if self.monitor_data is not None:
            self.monitor_data = self.monitor_data.json()
        else:
            self.__ui_logger(f"Failed to get monitor data for {self.monitor.monitor_id}", ERROR=True)
            self.error_message.setText(f"Failed to get monitor data for {self.monitor.monitor_id}")
            self.error_message.show()
            return
        pprint(self.monitor_data)
        # pprint(self.monitor_data)
        # print(f"monitor_ID: {self.monitor.monitor_id}")
        self.monitor_id_label.setText(str(self.monitor.monitor_id))
        try:
            self.serial_number_label.setText(str(self.monitor.serial_number))
        except AttributeError as e:
            self.__ui_logger(f"Failed to get serial number: {e}")
            self.error_message.setText(f"Error getting serial number: {e}")
            self.error_message.show()
        try:
            self.online_status_label.setText(
                str(self.monitor.monitor_details["monitor_status"]["connected"]))
        except (KeyError, TypeError) as e:
            self.__ui_logger(f"Failed to get monitor connection status: {e}")
            self.online_status_label.setText("Offline")
        try:
            self.current_run_label.setText(
                str(self.monitor_data["status"]["run"]))
        except (KeyError, TypeError) as e:
            self.current_run_label.setText("N/A")
            self.closeChannelMapRequest_btn.setEnabled(False)
            print("\n" + str(e) + "\n")
        try:
            self.timezone_label.setText(
                str(self.monitor.monitor_details["time_zone"]))
        except KeyError:
            self.timezone_label.setText("N/A")

        try:
            self.signal_check_status_label.setText(
                str(self.monitor.monitor_details["signal_check_status"]))
        except (KeyError,TypeError) as e:
            print(e)
            self.signal_check_status_label.setText("None")
            self.closeChannelMapRequest_btn.setEnabled(False)
        # self.closeChannelMapRequest_btn.setEnabled(True)
        try:
            if self.monitor.monitor_details["signal_check_status"] == "OK":
                self.__ui_logger(
                    f"Signal check status is: {self.monitor.monitor_details['signal_check_status']}, disabling closeChannelMap_btn")
                self.closeChannelMapRequest_btn.setEnabled(False)
        except KeyError as e:
                self.__ui_logger(
                    f"Signal check status is: None, disabling closeChannelMap_btn")
                self.closeChannelMapRequest_btn.setEnabled(False)


    def load_house(self):
        self.orig_channelMap = self.monitor.get_channelmap()
        # for check in checker:
        self.channelMap = self.convert_dcm_to_list(
            channelmap=self.orig_channelMap)
        pprint(self.channelMap)

        try:
            self.channelMap["CHANNEL_0"]
            self.CHANNEL_0_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_0"][0])
            self.CHANNEL_0_CT.setCurrentText(self.channelMap["CHANNEL_0"][1])
            self.CHANNEL_0_TYPE.setCurrentText(self.channelMap["CHANNEL_0"][2])
            self.CHANNEL_0_STRING = "{},{},{}".format(self.channelMap["CHANNEL_0"][0], self.channelMap["CHANNEL_0"][1],
                                                      self.channelMap["CHANNEL_0"][2])
        except KeyError:
            print("Setting CHANNEL_0 as default")
            self.channelMap.update({"CHANNEL_0": "V0,CT0,MAINS"})
            self.CHANNEL_0_VOLTAGE.setCurrentText("V0")
            self.CHANNEL_0_CT.setCurrentText("CT0")
            self.CHANNEL_0_TYPE.setCurrentText("MAINS")
            self.CHANNEL_0_STRING = "V0,CT0,MAINS"

        try:
            self.channelMap["CHANNEL_1"]
            try:
                self.CHANNEL_1_VOLTAGE.setCurrentText(
                    self.channelMap["CHANNEL_1"][0])
            except IndexError:
                self.CHANNEL_1_VOLTAGE.setCurrentText("V1")
            try:
                self.CHANNEL_1_CT.setCurrentText(self.channelMap["CHANNEL_1"][1])
            except IndexError:
                self.CHANNEL_1_CT.setCurrentText("CT2")
            try:
                self.CHANNEL_1_TYPE.setCurrentText(self.channelMap["CHANNEL_1"][2])
            except IndexError:
                self.CHANNEL_1_TYPE.setCurrentText("MAINS")
            try:
                self.CHANNEL_1_STRING = "{},{},{}".format(
                    self.channelMap["CHANNEL_1"][0], self.channelMap["CHANNEL_1"][1], self.channelMap["CHANNEL_1"][2])
            except IndexError:
                self.CHANNEL_1_STRING = "{},{},{}".format(
                    "V1", "CT1", "MAINS")
                
        except KeyError:
            self.channelMap.update({"CHANNEL_1": "V1,CT1,MAINS"})
            self.CHANNEL_1_VOLTAGE.setCurrentText("V1")
            self.CHANNEL_1_CT.setCurrentText("CT1")
            self.CHANNEL_1_TYPE.setCurrentText("MAINS")
            self.CHANNEL_1_STRING = self.channelMap["CHANNEL_1"]
        try:
            self.channelMap["CHANNEL_2"]
            self.CHANNEL_2_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_2"][0])
            self.CHANNEL_2_CT.setCurrentText(self.channelMap["CHANNEL_2"][1])
            self.CHANNEL_2_TYPE.setCurrentText(self.channelMap["CHANNEL_2"][2])
            self.CHANNEL_2_STRING = "{},{},{}".format(self.channelMap["CHANNEL_2"][0], self.channelMap["CHANNEL_2"][1],
                                                      self.channelMap["CHANNEL_2"][2])
        except KeyError:
            print("Setting Channel 2 as default")
            self.channelMap.update({"CHANNEL_2": "V0,CT2,NONE"})
            self.CHANNEL_2_VOLTAGE.setCurrentText("V0")
            self.CHANNEL_2_CT.setCurrentText("CT2")
            self.CHANNEL_2_TYPE.setCurrentText("NONE")
            self.CHANNEL_2_STRING = "V0,CT2,NONE"

        try:
            self.CHANNEL_3_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_3"][0])
            self.CHANNEL_3_CT.setCurrentText(self.channelMap["CHANNEL_3"][1])
            self.CHANNEL_3_TYPE.setCurrentText(self.channelMap["CHANNEL_3"][2])
            self.CHANNEL_3_STRING = "{},{},{}".format(
                self.channelMap["CHANNEL_3"][0], self.channelMap["CHANNEL_3"][1], self.channelMap["CHANNEL_3"][2])
        except KeyError:
            print("Setting Channel 3 as default")
            self.channelMap.update({"CHANNEL_3": "V1,CT3,NONE"})
            self.CHANNEL_3_VOLTAGE.setCurrentText("V1")
            self.CHANNEL_3_CT.setCurrentText("CT3")
            self.CHANNEL_3_TYPE.setCurrentText("NONE")
            self.CHANNEL_3_STRING = "V1,CT3,NONE"
        try:
            self.SOLAR_CONNECTED_TO_BREAKERS.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.channelMap.update({"SOLAR_CONNECTED_TO_BREAKERS": "0"})
            self.SOLAR_CONNECTED_TO_BREAKERS.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        try:
            self.EXPLICIT_CHANNEL_MAP.setCurrentText(
                self.channelMap["EXPLICIT_CHANNEL_MAP"])
        except KeyError:
            self.EXPLICIT_CHANNEL_MAP.setCurrentText("0")
            self.EXPLICIT_CHANNEL_MAP.setCurrentText(
                self.channelMap["EXPLICIT_CHANNEL_MAP"])
        try:
            self.AUX_PORT_MODE.setCurrentText(self.channelMap["AUX_PORT_MODE"])
        except KeyError:
            self.AUX_PORT_MODE.setCurrentText("ignore")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})
        try:
            self.generator_mode_comboBox.setCurrentText(self.channelMap["GENERATOR_MODE"])
        except KeyError:
            self.generator_mode_comboBox.setCurrentText("ignore")

    def update_strings(self):
        self.CHANNEL_0_STRING = "{},{},{}".format(self.CHANNEL_0_VOLTAGE.currentText(
        ), self.CHANNEL_0_CT.currentText(), self.CHANNEL_0_TYPE.currentText())
        self.CHANNEL_1_STRING = "{},{},{}".format(self.CHANNEL_1_VOLTAGE.currentText(
        ), self.CHANNEL_1_CT.currentText(), self.CHANNEL_1_TYPE.currentText())
        self.CHANNEL_2_STRING = "{},{},{}".format(self.CHANNEL_2_VOLTAGE.currentText(
        ), self.CHANNEL_2_CT.currentText(), self.CHANNEL_2_TYPE.currentText())
        self.CHANNEL_3_STRING = "{},{},{}".format(self.CHANNEL_3_VOLTAGE.currentText(
        ), self.CHANNEL_3_CT.currentText(), self.CHANNEL_3_TYPE.currentText())
        self.SCTB = self.SOLAR_CONNECTED_TO_BREAKERS.currentText()
        self.AUX = self.AUX_PORT_MODE.currentText()
        self.GENERATOR_MODE = self.generator_mode_comboBox.currentText()
        self.EXPLICIT_CM = self.EXPLICIT_CHANNEL_MAP.currentText()

    def update_channelmap(self):
        online_status = self.monitor.get_online_status()
        self.__ui_logger("Online Status: {}".format(online_status))
        if not online_status:
            # monitor offline
            self.error_message.setText(
                "h{} is offline, cannot update".format(self.monitor.monitor_id))
            self.error_message.show()
            return
        glossary = {"CHANNEL_0": self.CHANNEL_0_STRING,
                    "CHANNEL_1": self.CHANNEL_1_STRING,
                    "CHANNEL_2": self.CHANNEL_2_STRING,
                    "CHANNEL_3": self.CHANNEL_3_STRING}

        basic_glossary = {"AUX_PORT_MODE": self.AUX,
                          "SOLAR_CONNECTED_TO_BREAKERS": self.SCTB,
                          "EXPLICIT_CHANNEL_MAP": self.EXPLICIT_CM}
        if self.AUX == "generator":
            self.__ui_logger(f"Updating GENERATOR_MODE to {self.GENERATOR_MODE}")
            if self.GENERATOR_MODE != "ignore":
                basic_glossary.update({"GENERATOR_MODE": self.GENERATOR_MODE})
        to_update = {}
        for param, value in self.channelMap.items():
            if type(value) == list:
                to_str = ",".join([str(e) for e in value])
                self.channelMap.update({param: to_str})
        self.update_strings()
        for param in self.channelMap.keys():
            if param in glossary.keys():
                if self.channelMap[param] != glossary[param]:
                    check_ls = glossary[param].split(",")
                    if check_ls[2] == "NONE":
                        self.update_strings()
                        self.warning_message = QMessageBox()
                        self.warning_message.setText(
                            "Cannot set {} with NONE as type\nNo changes made.\nPlease use the Admin Console to remove middle port configuration!".format(param))
                        self.warning_message.show()
                        return

                    value = glossary[param]
                    to_update.update({param: value})
        for param in basic_glossary.keys():
            if param in basic_glossary.keys():
                if self.channelMap[param] != basic_glossary[param]:
                    self.__ui_logger("Updating: {} to {}".format(
                        param, basic_glossary[param]))
                    # print("UPDATE: {} to {}".format(param, basic_glossary[param]))
                    to_update.update({param: basic_glossary[param]})
        if len(to_update.keys()) > 0:
            sleep(2)
            self.error_message.setText(
                "Updated {} parameters successfully".format(len(to_update.keys())))
            self.error_message.show()
        else:
            sleep(2)
            self.error_message.setText(f"Detected {len(to_update.keys())} changes.")
            self.error_message.show()
        for param, value in to_update.items():
            print(param, value, self.monitor.update_channelmap(param, value))
            sleep(1.5)
        self.load_house()

        # voltage = current_values[0]
        # ct = current_values[1]
        # type = current_values[2]

    def convert_dcm_to_list(self, channelmap):
        output = {}
        for param, values in channelmap.items():
            if values.__contains__(","):
                values = values.split(",")
            output.update({param: values})
        return output

    def __load_old_channel_map(self):
        channel_map = self.channelMap
        try:
            self.ConfigPage_ComboBox_MainCTPolarity.setCurrentText(
                channel_map["MAIN_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.channelMap.update({"MAIN_CT_POLARITY_BACKWARDS": "0,0"})

        try:
            self.ConfigPage_ComboBox_SolarCTPolarity.setCurrentText(
                channel_map["SOLAR_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.channelMap.update({"SOLAR_CT_POLARITY_BACKWARDS": "0,0"})
            self.ConfigPage_ComboBox_SolarCTPolarity.setCurrentText(
                channel_map["SOLAR_CT_POLARITY_BACKWARDS"])

        try:
            self.ConfigPage_ComboBox_SwapMains.setCurrentText(
                channel_map["LEFT_RIGHT_MAINS_SWAPPED"])
        except KeyError:
            self.channelMap.update({"LEFT_RIGHT_MAINS_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SwapMains.setCurrentText(
                channel_map["LEFT_RIGHT_MAINS_SWAPPED"])

        try:
            self.ConfigPage_ComboBox_SwapSolar.setCurrentText(
                channel_map["LEFT_RIGHT_SOLAR_SWAPPED"])
        except KeyError:
            self.channelMap.update({"LEFT_RIGHT_SOLAR_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SwapSolar.setCurrentText(
                channel_map["LEFT_RIGHT_SOLAR_SWAPPED"])
        try:
            # self.ConfigPage_ComboBox_UsingSolar.setCurrentText(
            #     channel_map["USING_SOLAR"])
            if channel_map["USING_SOLAR"] == 1:
                self.channelMap.update({"AUX_PORT_MODE": "solar"})
            else:
                # if len(channel_map["aux_port_mode"]) > 0:
                #     self.ConfigPage_ComboBox_UsingSolar.setCurrentText(
                #         channel_map["aux_port_mode"])
                # else:
                self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        except KeyError:
            self.__ui_logger("KEY_ERROR: USING_SOLAR")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        try:
            self.channelMap.update(
                {"AUX_PORT_MODE": channel_map["AUX_PORT_MODE"]})
        except KeyError:
            self.__ui_logger("KEY ERROR: AUX_PORT_MODE")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        try:
            self.ConfigPage_ComboBox_SolarConnectedToBreakers.setCurrentText(
                channel_map["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.__ui_logger(
                "SOLAR_CONNECTED_TO_BREAKERS not found, setting to default (0).")
            self.ConfigPage_ComboBox_SolarConnectedToBreakers.setCurrentText(
                "0")

        try:
            self.ConfigPage_LineEdit_FrameRate.setText(
                channel_map["FRAME_RATE"])
        except KeyError:
            self.__ui_logger("KEY_ERROR: FRAME_RATE not set. Setting to 60...")
            self.ConfigPage_LineEdit_FrameRate.setText("60")

        try:
            self.ConfigPage_LineEdit_NumChannels.setText(
                channel_map["NUM_MAINS_CHANNELS"])
        except KeyError:
            self.__ui_logger(
                "KEY_ERROR: NUM_CHANNELS not set, setting to 2...")
            self.ConfigPage_LineEdit_NumChannels.setText("2")

        try:
            self.ConfigPage_ComboBox_SolarMainsSwapped.setCurrentText(
                channel_map["SOLAR_MAINS_SWAPPED"])
        except KeyError:
            self.channelMap.update({"SOLAR_MAINS_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SolarMainsSwapped.setCurrentText(
                channel_map["SOLAR_MAINS_SWAPPED"])
        self.__ui_logger("Loaded current channel map")

    def __update_old_channelmap(self):
        # TODO
        glossary = {"LEFT_RIGHT_MAINS_SWAPPED": self.ConfigPage_ComboBox_SwapMains.currentText(),
                    "LEFT_RIGHT_SOLAR_SWAPPED": self.ConfigPage_ComboBox_SwapSolar.currentText(),
                    "MAIN_CT_POLARITY_BACKWARDS": self.ConfigPage_ComboBox_MainCTPolarity.currentText(),
                    "SOLAR_CT_POLARITY_BACKWARDS": self.ConfigPage_ComboBox_SolarCTPolarity.currentText(),
                    "USING_SOLAR": self.ConfigPage_ComboBox_UsingSolar.currentText(),
                    "SOLAR_CONNECTED_TO_BREAKERS": self.ConfigPage_ComboBox_SolarConnectedToBreakers.currentText(),
                    "FRAME_RATE": self.ConfigPage_LineEdit_FrameRate.text(),
                    "NUM_MAINS_CHANNELS": self.ConfigPage_LineEdit_NumChannels.text(),
                    "SOLAR_MAINS_SWAPPED": self.ConfigPage_ComboBox_SolarMainsSwapped.currentText(),
                    "AUX_PORT_MODE": self.channelMap["AUX_PORT_MODE"]}
        to_update = {}
        for parameter, value in glossary.items():
            try:
                if parameter not in self.channelMap.keys():
                    print(parameter)
            except KeyError as e:
                print("KEY_ERROR: {}".format(e))
        pprint(to_update)

    def load_old_channel_map(self):
        try:  # main polarity
            self.main_polarity.setCurrentText(
                self.channelMap["MAIN_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.main_polarity.setCurrentText("0,0")
            self.channelMap.update({"MAIN_CT_POLARITY_BACKWARDS": "0,0"})

        try:  # Solar polarity
            self.solar_polarity.setCurrentText(
                self.channelMap["SOLAR_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.solar_polarity.setCurrentText("0,0")
            self.channelMap.update({"SOLAR_CT_POLARITY_BACKWARDS": "0,0"})

        try:  # Swap mains
            self.swap_mains.setCurrentText(
                self.channelMap["LEFT_RIGHT_MAINS_SWAPPED"])
        except KeyError:
            self.swap_mains.setCurrentText("0")
            self.channelMap.update({"LEFT_RIGHT_MAINS_SWAPPED": "0"})

        try:  # Swap Solar
            self.swap_solar.setCurrentText(
                self.channelMap["LEFT_RIGHT_SOLAR_SWAPPED"])
        except KeyError:
            self.swap_solar.setCurrentText("0")
            self.channelMap.update({"LEFT_RIGHT_SOLAR_SWAPPED": "0"})

        try:  # sctb
            self.sctb.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.sctb.setCurrentText("0")
            self.channelMap.update({"SOLAR_CONNECTED_TO_BREAKERS": "0"})

        try:  # frame rate
            self.frame_rate.setText(self.channelMap["FRAME_RATE"])
        except KeyError:
            self.frame_rate.setText("60")
            self.channelMap.update({"FRAME_RATE": "60"})

        try:  # number of main channels
            self.num_main_channels.setText(
                self.channelMap["NUM_MAINS_CHANNELS"])
        except KeyError:
            self.num_main_channels.setText("2")
            self.channelMap.update({"NUM_MAINS_CHANNELS": "2"})

        try:  # Solar mains swapped
            self.solar_mains_swapped.setCurrentText(
                self.channelMap["SOLAR_MAINS_SWAPPED"])
        except KeyError:
            self.solar_mains_swapped.setCurrentText("0")
            self.channelMap.update({"SOLAR_MAINS_SWAPPED": "0"})

        try:  # aux_port_mode
            if "USING_SOLAR" in self.channelMap.keys():
                if self.channelMap["USING_SOLAR"] == "1":
                    self.aux_port_mode.setCurrentText("solar")
                if "AUX_PORT_MODE" in self.channelMap.keys():
                    self.aux_port_mode.setCurrentText(
                        self.channelMap["AUX_PORT_MODE"])
            else:
                self.aux_port_mode.setCurrentText(
                    self.channelMap["AUX_PORT_MODE"])
        except KeyError:
            self.aux_port_mode.setCurrentText("ignore")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

    # def update_monitor_config(self, channel_map):
    #     pass

    def add_defaults_to_current_channelmap(self):
        DEFAULT = {"MAIN_CT_POLARITY_BACKWARDS": "0,0",
                   "SOLAR_CT_POLARITY_BACKWARDS": "0,0",
                   "LEFT_RIGHT_MAINS_SWAPPED": "0",
                   "LEFT_RIGHT_SOLAR_SWAPPED": "0",
                   "AUX_PORT_MODE": "ignore",
                   "SOLAR_MAINS_SWAPPED": "0",
                   "FRAME_RATE": "60",
                   "NUM_MAINS_CHANNELS": "2"}

    def update_old_channel_map(self):
        online_status = self.monitor.get_online_status()
        self.__ui_logger("Online Status: {}".format(online_status))
        if not online_status:
            # monitor offline
            self.error_message.setText(
                "h{} is offline, cannot update".format(self.monitor.monitor_id))
            self.error_message.show()
            return
        updated_channel_map = {
            "MAIN_CT_POLARITY_BACKWARDS": self.main_polarity.currentText(),
            "SOLAR_CT_POLARITY_BACKWARDS": self.solar_polarity.currentText(),
            "LEFT_RIGHT_MAINS_SWAPPED": self.swap_mains.currentText(),
            "LEFT_RIGHT_SOLAR_SWAPPED": self.swap_solar.currentText(),
            "AUX_PORT_MODE": self.aux_port_mode.currentText(),
            "FRAME_RATE": self.frame_rate.text(),
            "NUM_MAINS_CHANNELS": self.num_main_channels.text(),
            "SOLAR_CONNECTED_TO_BREAKERS": self.sctb.currentText(),
            "SOLAR_MAINS_SWAPPED": self.solar_mains_swapped.currentText()
        }
        updater = {}
        for parameter, original_value in self.channelMap.items():
            try:
                new_value = updated_channel_map[parameter]
                if new_value != original_value:
                    updater.update({parameter: new_value})
            except KeyError as e:
                self.__ui_logger("Key Error for {}, KEY: {}".format(
                    self.monitor.monitor_id, e))
                pass
        for parameter, value in updater.items():
            self.__ui_logger("Updating h{}: {}: {}".format(
                self.monitor.monitor_id, parameter, value))
            self.monitor.update_channelmap(parameter, value)
            self.channelMap.update({parameter: value})
            sleep(1)
        self.error_message.setText("Updated {} values for h{}".format(
            len(updater.keys()), self.monitor.monitor_id))
        self.error_message.show()

    def __ui_logger(self, message, INFO=True, WARN=False, ERROR=False):
        datestamp = datetime.utcnow()
        severity = "INFO"
        if INFO:
            logging.info("{} -- {}".format(datestamp, message))
        if WARN:
            severity = "WARN"
            logging.warning("{} -- {}".format(datestamp, message))
        elif ERROR:
            logging.error("{} -- {}".format(datestamp, message))
            severity = "ERROR"
        message = "{} : {} -- {}".format(severity, datestamp, message)
        self.LoggingPage_TextEdit_Console.append(message)

    def get_setup_logs(self, result):
        self.log_dates_list.clear()
        self.monitor = Monitor(self.monitor_id, Auth().token)
        setup_logs = self.monitor.get_setup_logs()
        logs = {}
        for log in setup_logs:
            status = log["setup_result"]
            status_list = [x for x in status]
            if result == "All":
                logs.update({log["date_created"]: log})
            elif result == "Failure":
                if "E" in status_list and status != "SUCCESS":
                    logs.update({log["date_created"]: log})
            elif result == "Success":
                if log["setup_result"] == "SUCCESS":
                    logs.update({log["date_created"]: log})
        for key in logs.keys():
            self.log_dates_list.addItem(easy_dates(
                key) + " || Result: " + logs[key]["setup_result"])
        return logs

    def select_setup_log(self, logs, mode):
        self.client_log_output.setText("")
        collector = []
        for log in logs.values():
            collector.append(log)
        selected = self.log_dates_list.currentRow()
        final = ""
        if mode == "logs":
            for key, value in collector[selected].items():
                if key != "date_created":
                    if key == "log_body":
                        final = str(final) + "\nSetup Attempt:" + \
                            "\n" + str(value) + "\n"
                    final = str(final) + str(key) + ": " + str(value) + "\n"
            final = final.replace("log_body:", "\nSetup Attempt:\n")
        elif mode == "network":
            final = self.test_result_to_str(logs)

        self.client_log_output.setText(final)

    def get_network_tests(self, result):
        self.log_dates_list.clear()
        self.monitor = Monitor(self.monitor_id, Auth().token)
        network_tests = self.monitor.get_network_tests()
        tests = {}
        for test in network_tests:
            if result == "All":
                tests.update({test["date_modified"]: test})
            elif result == "Success":
                if test["result"] == "p":
                    tests.update({test["date_modified"]: test})
            elif result == "Failure":
                if test["result"] == "f":
                    tests.update({test["date_modified"]: test})
                elif test["result"] == "w":
                    tests.update({test["date_modified"]: test})

        for key in tests.keys():
            test_result = tests[key]["result"]
            if test_result == "p":
                test_result = "Passed"
            elif test_result == "w":
                test_result = "Warning"
            elif test_result == "f":
                test_result = "Failure"
            self.log_dates_list.addItem(easy_dates(
                key) + " || Result: {}".format(test_result))
        return tests

    def close_channel_map(self):
        result = self.monitor.close_channelmap_in_signal_check()
        if result is not None:
            closed_run = result["run"]
            self.error_message.setText(
                f"h{self.monitor.monitor_id}: Closed signal check on run: {closed_run}")
            self.error_message.setWindowTitle("Success")
            self.error_message.show()
            self.__ui_logger(
                f"h{self.monitor.monitor_id}: Closed signal check on run: {closed_run}. Current run: {self.monitor.current_run}")
        else:
            self.__ui_logger(
                f"h{self.monitor.monitor_id}: Could not not channel map for latest bad run: {self.monitor.current_run - 2}, Current run: {self.monitor.current_run}")
            self.error_message.setText("Could not close channel map!")
            self.error_message.setWindowTitle("Error!")
            self.error_message.show()

    def test_result_to_str(self, tests):
        tmp = {}
        for t in tests.values():
            for key, value in t.items():
                tmp.update({key: value})
        test = tmp["tests"]
        result = tmp["result"]
        if result == "p":
            result = "Passed"
        elif result == "f":
            result = "Failed"
        elif result == "w":
            result = "Warning"
        output = "Overall Result: {}\n\n".format(result) + test_parser(test)

        return output


def test_parser(tests):
    output = ""
    for test in tests:
        for name, value in test.items():
            if name == "result":
                if value == "p":
                    value = "Passed"
                elif value == "w":
                    value = "Warning"
                elif value == "f":
                    value == "Failed"
            name = name.replace("_", " ").capitalize()
            output = output + "    " + str(name) + ": " + str(value) + "\n"
        output = output + "\n"
        # for name, value in test.items():
        #     if name == "result":
        #         if value == "p":
        #             result = "Passed"
        #         elif value == "f":
        #             result = "Failed"
        #         elif value == "w":
        #             result = "Warning"
        #     elif name == "message":
        #         message = value
    return output


def easy_dates(date):
    date = parser.parse(date)
    date_str = date.strftime("%m/%d/%y %I:%M:%S %Z")
    return date_str


# def main():
#     app = QApplication(sys.argv)
#     form = MainWindow()
#     form.show()
#     app.exec_()


# if __name__ == '__main__':
#     main()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
