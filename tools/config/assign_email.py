from Sense.Api import Monitor, Auth
from pprint import pprint
import argparse

DEFAULT_PASSWORD = "Sense!23"


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("monitor_id")

    return parser.parse_args()


def main():
    args = parse_opts()
    monitor = Monitor(args.monitor_id, Auth().token)
    data = monitor.init_info
    if "time_zone" not in data.keys():
        status = monitor.update_timezone("America/New_York")
        if status > 203:
            print(
                f"Could not update timezone to create finalize monitor creation.. Status code: {status}")
        else:
            print(
                "Successfully set default timezone, please update to the correct timezone after!")
    if "account" not in data.keys():
        email = f"{monitor.monitor_serial}@sense.com"
        if email:
            response = monitor.associate_user_to_serial(
                email=email, password=DEFAULT_PASSWORD)
            if response.status_code < 205:
                print(
                    f"Successfully associated h{monitor.monitor_id} to email: {email} and password: {DEFAULT_PASSWORD}")
            else:
                print(
                    f"Unable to update h{monitor.monitor_id}, Error: {response.json()}")
        else:
            print(
                f"Unable to get serial number for h{monitor.monitor_id}...")
    else:
        print(
            f"Did not update monitor: h{monitor.monitor_id}, 'account' value found.")


main()
