import argparse
from Sense.Api import Monitor, Auth

def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("-t", "--timezone",
                        type=str, choices=["est", "cst", "pst", "mst", "adt"], required=False, dest="timezone")
    parser.add_argument("-c", "--current", action="store_true", default=False, dest="current")
    parser.add_argument("--no-dst", action="store_true", default=False, dest="dst")
    parser.add_argument("--manual", dest="manual", default=None)
    args = parser.parse_args()
    return args


def main():
    args = parse_opts()
    auth = Auth().get_token()
    monitor = Monitor(args.house, auth)
    if args.current:
        timezone = monitor.get_timezone()
        print("Monitor: h{} timezone is set to {}".format(monitor.monitor_id, timezone))
        exit()
    tz = args.timezone
    if args.manual is not None:
        print("Manually updating h{} to: {}".format(monitor.monitor_id, args.manual))
        response = monitor.update_timezone(args.manual)
        if response == 200:
            print("Succesfully updated h{} to timezone: {}".format(monitor.monitor_id, args.manual))
        else:
            print("Failed to update h{} to timezone: {}... \nError code:".format(monitor.monitor_id, args.manual, r))
        exit()
    CHOICES = {"est": "America/New_York",
            "cst": "America/Chicago",
            "mst": "America/Denver",
            "pst": "America/Los_Angeles",
            "adt": "America/Halifax"}
    if tz in CHOICES.keys():
        timezone = CHOICES[tz]
        if args.dst:
            DST_CHOICES = {"est": "America/Montserrat",
                "mst": "America/Creston",
                "pst": "America/Los_Angeles",
                "adt": "America/Guyana"}
            timezone = DST_CHOICES[tz]
        r = monitor.update_timezone(timezone)
        if r == 200:
            print("Updated h{} to timezone: {}".format(monitor.monitor_id, timezone))
        else:
            print("Failed to update h{} to timezone: {}... \nError code:".format(monitor.monitor_id, timezone, r))


main()
