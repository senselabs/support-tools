import requests
import sys
import os
from getpass import getpass
import pymysql
from Sense.Api import Auth, Monitor
import argparse

house = sys.argv[1]


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house")
    args = parser.parse_args()
    return args


connection = pymysql.connect(host='db300.cmshntoqbhnc.us-east-1.rds.amazonaws.com',
                             user='models',
                             password='modelsdb',
                             db='models',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


def getChannelCheckValue(house):
    try:
        with connection.cursor() as cursor:
            print('update house ' + str(house))
            sql = "select house, channel_check from models.house_info where house =" + \
                str(house)
            cursor.execute(sql)
            data = cursor.fetchone()
            print(data)
    finally:
        connection.close()


def updateChannelCheckValue(house):
    try:
        with connection.cursor() as cursor:
            print('updating house ' + str(house))
            sql = "update models.house_info set channel_check ='OK' where house =" + \
                str(house)
            cursor.execute(sql)
            connection.commit()
    finally:
        connection.close()


def forceChannelCheck(house):
    url = "https://api.sense.com/api/v1/admin/monitors/{}/signal_check".format(
        house)

    payload = "status=OK"
    headers = {
        'Authorization': "bearer " + Auth().token,
        'Content-Type': "application/x-www-form-urlencoded",
    }
    response = requests.request("POST", url, data=payload, headers=headers)
    if response.status_code == 200:
        print("Sucessfully pushed h{} through signal check.".format(house))
    else:
        print("Error: {}".format(response.reason))


def main():
    house = parse_opts().house
    house = Monitor(house, Auth().token).monitor_id
    updateChannelCheckValue(house)
    forceChannelCheck(house)


main()
