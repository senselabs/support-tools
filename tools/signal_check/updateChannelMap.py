import argparse
from time import sleep
from pprint import pprint
from Sense.Api import Auth, Monitor
import logging

logging.captureWarnings(True)

one = ["0", "1"]
two = ["0,0", "0,1", "1,0", "1,1"]
aux_possible = ["ignore", "solar", "generator", "split_service", "octopus"]

arg_choices = {"aux": "AUX_PORT_MODE",
                     "sctb": "SOLAR_CONNECTED_TO_BREAKERS",
                     "main_polarity": "MAIN_CT_POLARITY_BACKWARDS",
                     "solar_polarity": "SOLAR_CT_POLARITY_BACKWARDS",
                     "swap_mains": "LEFT_RIGHT_MAINS_SWAPPED",
                     "swap_solar": "LEFT_RIGHT_SOLAR_SWAPPED",
                     "num_main_channels": "NUM_MAINS_CHANNELS",
                     "frame_rate": "FRAME_RATE",
                     "ecm": "EXPLICIT_CHANNEL_MAP", 
                     "solar_mains_swapped": "SOLAR_MAINS_SWAPPED",
                     "solar": "USING_SOLAR"}


VALUE_CHECKER = {
    "aux": aux_possible,
    "solar": one,
    "sctb": one,
    "main_polarity": two,
    "solar_polarity": two,
    "swap_mains": one,
    "swap_solar": one,
    "solar_mains_swapped": one,
    "ecm": one,
    "frame_rate": ["50", "60"],
    "num_main_channels": ["1", "2"]
}


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("parameter", type=str, nargs="?", default=None,
                        choices=arg_choices.keys())
    parser.add_argument("value", type=str, nargs="?", default=None)

    args = parser.parse_args()
    return args

def notify_software(monitor):
    software = monitor.get_software_version()
    comparable = float(software[0:4])
    if comparable >= 1.26:
        print("\nNote: h{} DOES require Aux Port syntax in ChannelMap\n".format(monitor.monitor_id))
    else:
        print("\nNote: h{} does NOT use Aux Port syntax in ChannelMap\n".format(monitor.monitor_id))
    return software


def check_software_version(monitor):
    # If sw >= 1.26 then it requires aux params
    software = monitor.get_software_version()
    comparable = float(software[0:4])
    if comparable >= 1.26:
        # This uses aux port instead of solar
        PARAMETER_CHOICES = {"aux": "AUX_PORT_MODE",
                     "sctb": "SOLAR_CONNECTED_TO_BREAKERS",
                     "main_polarity": "MAIN_CT_POLARITY_BACKWARDS",
                     "solar_polarity": "SOLAR_CT_POLARITY_BACKWARDS",
                     "swap_mains": "LEFT_RIGHT_MAINS_SWAPPED",
                     "swap_solar": "LEFT_RIGHT_SOLAR_SWAPPED",
                     "num_main_channels": "NUM_MAINS_CHANNELS",
                     "ecm": "EXPLICIT_CHANNEL_MAP",
                     "frame_rate": "FRAME_RATE",
                     "solar_mains_swapped": "SOLAR_MAINS_SWAPPED"}
    else:
        PARAMETER_CHOICES = {"solar": "USING_SOLAR",
                     "sctb": "SOLAR_CONNECTED_TO_BREAKERS",
                     "main_polarity": "MAIN_CT_POLARITY_BACKWARDS",
                     "solar_polarity": "SOLAR_CT_POLARITY_BACKWARDS",
                     "swap_mains": "LEFT_RIGHT_MAINS_SWAPPED",
                     "swap_solar": "LEFT_RIGHT_SOLAR_SWAPPED",
                     "num_main_channels": "NUM_MAINS_CHANNELS",
                     "frame_rate": "FRAME_RATE",
                     "solar_mains_swapped": "SOLAR_MAINS_SWAPPED"}
    return PARAMETER_CHOICES


def update(monitor, parameter, value):
    # monitor needs to be object
    PARAMETER_CHOICES = check_software_version(monitor)
    if parameter in PARAMETER_CHOICES.keys():
        user_param = parameter
        # Valid parameter
        if value in VALUE_CHECKER[parameter]:
            # Valid value for parameter
            parameter = PARAMETER_CHOICES[parameter]
            # re-assign parameter from user-friendly commandline argument to what channelmap expects
            update_map_r = monitor.update_channelmap(parameter, value)
            success_count = 0
            for request, response in update_map_r.items():
                if response > 201:
                    print(f"Updating h{monitor.monitor_id}: {request} failed with error code: {response}")
                else:
                    success_count += 1
            if success_count == 3:
                print(f"Updated h{monitor.monitor_id}: {parameter} to {value}")
        else:
            # Value is not valid for selected param
            print("Value: {} is not supported for parameter".format(value))
    else:
        # param is not valid
        print("Parameter: {} not found in list of choices.".format(parameter))


if __name__ == "__main__":
    args = parse_opts()
    auth = Auth().get_token()
    monitor = Monitor(args.house, auth)
    if monitor.get_online_status() is False:
        print(f"h{monitor.monitor_id} is offline. Please try again later.")
        exit()
    if args.parameter is None and args.value is None:
        current = monitor.get_channelmap()
        pprint(current)
        notify_software(monitor)
    else:
        try:
            update(monitor, args.parameter, args.value)
        except TypeError:
            print("Please be sure to include both parameter and value")
