from PyQt5 import QtGui, QtWidgets, uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject, pyqtSignal
from pprint import pprint
import sys
import ui
from time import sleep
from Sense.Api import Auth, Monitor
import subprocess
from datetime import datetime
from os.path import expanduser, exists
import logging

logging.basicConfig(filename='log.log', level=logging.INFO)


class MainWindow(QTabWidget, ui.Ui_TabWidget):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        error_message = QMessageBox()
        self.error_message = error_message

        # self.CHANNEL_0_CT, self.CHANNEL_0_TYPE, self.CHANNEL_0_VOLTAGE, self.CHANNEL_1_CT, self.CHANNEL_1_TYPE, self.CHANNEL_1_VOLTAGE, self.CHANNEL_2_CT, self.CHANNEL_2_TYPE, self.CHANNEL_3_CT, self.CHANNEL_3_TYPE, self.CHANNEL_3_VOLTAGE,
        buttons = [self.loadHouse, self.updateHouse,
                   self.DataPage_PushButton_LoadData, self.ConfigPage_PushButton_PushChanges, self.authenticate, self.check_token, self.remove_token]
        for button in buttons:
            if button.clicked.connect(self.PageListener):
                pass

    def PageListener(self):
        listener = self.sender()
        # print(listener)
        matcher = listener.objectName()
        if matcher == "loadHouse":
            try:
                self.create_monitor(self.house_input.text())
                # if self.house_input.text() is not None:
                if self.determine_which() == "1":  # This is DCM
                    self.load_house()
                else:
                    self.error_message.setText(
                        "h{} does not use DCM, use Old Syntax tab".format(self.house_input.text()))
                    self.error_message.show()
            except KeyError:
                self.error_message.setText(
                    f"h{self.house_input.text()} does not exist!")
                self.error_message.show()

            # elif self.DataPage_LineEdit_House.text() is not None:
        if matcher == "DataPage_PushButton_LoadData":
            try:
                self.create_monitor(self.monitor_id.text())
                if self.determine_which() == "0":  # Old syntax
                    self.load_old_channel_map()
                else:
                    self.error_message.setText("h{} uses DCM, use New Syntax tab".format(
                        self.monitor_id.text()))
                    self.error_message.show()
            except KeyError:
                self.error_message.setText(
                    "h{} does not exist!".format(self.monitor_id.text()))
                self.error_message.show()

        if matcher == "updateHouse":
            if self.determine_which() == "1":
                self.update_strings()
                self.update_channelmap()
            else:
                self.error_message.setText("Cannot update, use Old Syntax!")
                self.error_message.show()
        if matcher == "ConfigPage_PushButton_PushChanges":
            if self.determine_which() == "0":
                # self.create_monitor(self.monitor_id.text())
                self.update_old_channel_map()
                sleep(1)
                self.load_old_channel_map()
        if matcher == "authenticate":
            self.authenticate_user()
        if matcher == "check_token":
            self.test_auth()
        if matcher == "remove_token":
            self.remove_auth_token()

    def authenticate_user(self):
        try:
            token = Auth(require_input=False, email=self.email.text(),
                         password=self.password.text()).token
            if len(token) > 0:
                self.__ui_logger("Stored token: {}".format(token))
                self.error_message.setText("Generated Authentication Token")
                self.error_message.show()
            else:
                self.__ui_logger("Couldn't generate auth token")
                self.error_message.setText(
                    "Could not generate token, check credentials")
                self.error_message.show()
        except KeyError:
            self.error_message.setText(
                "Could not successfully authenticate you.\nEnsure email/password combo are correct.")
            self.error_message.show()
            self.__ui_logger("Validation failed")

    def remove_auth_token(self):
        try:
            rm = Auth(require_input=False).remove_auth()
            if rm:
                self.__ui_logger("Removed auth token")
                self.error_message.setText(
                    "Removed Authentication Token.\nPlease re-run Authenicate to generate")
                self.error_message.show()
            else:
                self.__ui_logger("Failed to remove auth token")
                self.error_message.setText("Failed to remove authentication.")
                self.error_message.show()
        except KeyError as e:
            self.__ui_logger("Failed to remove token: {}".format(e))
            self.error_message.setText("No token saved, re-run authentication")
            self.error_message.show()

    def test_auth(self):
        try:
            token_valid = Auth(require_input=False).test_auth()
            if token_valid:
                self.error_message.setText("Token is valid.")
                self.__ui_logger("Token validity: {}".format(token_valid))
                self.error_message.show()
            else:
                self.error_message.setText("Token invalid! Re-authenticate")
                self.__ui_logger("Invalid auth token: {}".format(token_valid))
                self.error_message.show()
        except KeyError as e:
            self.__ui_logger("Failed to check token: {}".format(e))
            self.error_message.setText("No token saved, re-run authentication")
            self.error_message.show()

    def determine_which(self):
        if "EXPLICIT_CHANNEL_MAP" in self.channelMap.keys():
            # if 1 it means we are using explicit cm
            return self.channelMap["EXPLICIT_CHANNEL_MAP"]
        else:
            return "0"

    def create_monitor(self, monitor_id):
        self.monitor = Monitor(monitor_id, Auth().token)
        self.channelMap = self.monitor.get_channelmap()

    def load_house(self):
        to_update = [self.CHANNEL_0_CT, self.CHANNEL_0_TYPE, self.CHANNEL_0_VOLTAGE, self.CHANNEL_1_CT, self.CHANNEL_1_TYPE,
                     self.CHANNEL_1_VOLTAGE, self.CHANNEL_2_CT, self.CHANNEL_2_TYPE, self.CHANNEL_3_CT, self.CHANNEL_3_TYPE, self.CHANNEL_3_VOLTAGE]
        # checker = ["CHANNEL_0", "CHANNEL_1", "CHANNEL_2", "CHANNEL_3"]
        # self.monitor = Monitor(str(monitor_id), Auth().token)
        self.orig_channelMap = self.monitor.get_channelmap()
        # for check in checker:
        self.channelMap = self.convert_dcm_to_list(
            channelmap=self.orig_channelMap)

        try:
            self.channelMap["CHANNEL_0"]
            self.CHANNEL_0_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_0"][0])
            self.CHANNEL_0_CT.setCurrentText(self.channelMap["CHANNEL_0"][1])
            self.CHANNEL_0_TYPE.setCurrentText(self.channelMap["CHANNEL_0"][2])
            self.CHANNEL_0_STRING = "{},{},{}".format(self.channelMap["CHANNEL_0"][0], self.channelMap["CHANNEL_0"][1],
                                                      self.channelMap["CHANNEL_0"][2])
        except KeyError:
            print("Setting CHANNEL_0 as default")
            self.channelMap.update({"CHANNEL_0": "V0,CT0,MAINS"})
            self.CHANNEL_0_VOLTAGE.setCurrentText("V0")
            self.CHANNEL_0_CT.setCurrentText("CT0")
            self.CHANNEL_0_TYPE.setCurrentText("MAINS")
            self.CHANNEL_0_STRING = "V0,CT0,MAINS"

        try:
            self.channelMap["CHANNEL_1"]
            self.CHANNEL_1_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_1"][0])
            self.CHANNEL_1_CT.setCurrentText(self.channelMap["CHANNEL_1"][1])
            self.CHANNEL_1_TYPE.setCurrentText(self.channelMap["CHANNEL_1"][2])
            self.CHANNEL_1_STRING = "{},{},{}".format(
                self.channelMap["CHANNEL_1"][0], self.channelMap["CHANNEL_1"][1], self.channelMap["CHANNEL_1"][2])
        except KeyError:
            self.channelMap.update({"CHANNEL_1": "V1,CT1,MAINS"})
            self.CHANNEL_1_VOLTAGE.setCurrentText("V1")
            self.CHANNEL_1_CT.setCurrentText("CT1")
            self.CHANNEL_1_TYPE.setCurrentText("MAINS")
            self.CHANNEL_1_STRING = self.channelMap["CHANNEL_1"]
        try:
            self.channelMap["CHANNEL_2"]
            self.CHANNEL_2_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_2"][0])
            self.CHANNEL_2_CT.setCurrentText(self.channelMap["CHANNEL_2"][1])
            self.CHANNEL_2_TYPE.setCurrentText(self.channelMap["CHANNEL_2"][2])
            self.CHANNEL_2_STRING = "{},{},{}".format(self.channelMap["CHANNEL_2"][0], self.channelMap["CHANNEL_2"][1],
                                                      self.channelMap["CHANNEL_2"][2])
        except KeyError:
            print("Setting Channel 2 as default")
            self.channelMap.update({"CHANNEL_2": "V0,CT2,NONE"})
            self.CHANNEL_2_VOLTAGE.setCurrentText("V0")
            self.CHANNEL_2_CT.setCurrentText("CT2")
            self.CHANNEL_2_TYPE.setCurrentText("NONE")
            self.CHANNEL_2_STRING = "V0,CT2,NONE"

        try:
            self.CHANNEL_3_VOLTAGE.setCurrentText(
                self.channelMap["CHANNEL_3"][0])
            self.CHANNEL_3_CT.setCurrentText(self.channelMap["CHANNEL_3"][1])
            self.CHANNEL_3_TYPE.setCurrentText(self.channelMap["CHANNEL_3"][2])
            self.CHANNEL_3_STRING = "{},{},{}".format(
                self.channelMap["CHANNEL_3"][0], self.channelMap["CHANNEL_3"][1], self.channelMap["CHANNEL_3"][2])
        except KeyError:
            print("Setting Channel 3 as default")
            self.channelMap.update({"CHANNEL_3": "V1,CT3,NONE"})
            self.CHANNEL_3_VOLTAGE.setCurrentText("V1")
            self.CHANNEL_3_CT.setCurrentText("CT3")
            self.CHANNEL_3_TYPE.setCurrentText("NONE")
            self.CHANNEL_3_STRING = "V1,CT3,NONE"
        try:
            self.SOLAR_CONNECTED_TO_BREAKERS.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.channelMap.update({"SOLAR_CONNECTED_TO_BREAKERS": "0"})
            self.SOLAR_CONNECTED_TO_BREAKERS.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        try:
            self.EXPLICIT_CHANNEL_MAP.setCurrentText(
                self.channelMap["EXPLICIT_CHANNEL_MAP"])
        except KeyError:
            self.EXPLICIT_CHANNEL_MAP.setCurrentText("0")
            self.EXPLICIT_CHANNEL_MAP.setCurrentText(
                self.channelMap["EXPLICIT_CHANNEL_MAP"])
        try:
            self.AUX_PORT_MODE.setCurrentText(self.channelMap["AUX_PORT_MODE"])
        except KeyError:
            self.AUX_PORT_MODE.setCurrentText("ignore")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

    def update_strings(self):
        self.CHANNEL_0_STRING = "{},{},{}".format(self.CHANNEL_0_VOLTAGE.currentText(
        ), self.CHANNEL_0_CT.currentText(), self.CHANNEL_0_TYPE.currentText())
        self.CHANNEL_1_STRING = "{},{},{}".format(self.CHANNEL_1_VOLTAGE.currentText(
        ), self.CHANNEL_1_CT.currentText(), self.CHANNEL_1_TYPE.currentText())
        self.CHANNEL_2_STRING = "{},{},{}".format(self.CHANNEL_2_VOLTAGE.currentText(
        ), self.CHANNEL_2_CT.currentText(), self.CHANNEL_2_TYPE.currentText())
        self.CHANNEL_3_STRING = "{},{},{}".format(self.CHANNEL_3_VOLTAGE.currentText(
        ), self.CHANNEL_3_CT.currentText(), self.CHANNEL_3_TYPE.currentText())
        self.SCTB = self.SOLAR_CONNECTED_TO_BREAKERS.currentText()
        self.AUX = self.AUX_PORT_MODE.currentText()
        self.EXPLICIT_CM = self.EXPLICIT_CHANNEL_MAP.currentText()

    def update_channelmap(self):
        online_status = self.monitor.get_online_status()
        self.__ui_logger("Online Status: {}".format(online_status))
        if not online_status:
            # monitor offline
            self.error_message.setText(
                "h{} is offline, cannot update".format(self.monitor.monitor_id))
            self.error_message.show()
            return
        glossary = {"CHANNEL_0": self.CHANNEL_0_STRING,
                    "CHANNEL_1": self.CHANNEL_1_STRING,
                    "CHANNEL_2": self.CHANNEL_2_STRING,
                    "CHANNEL_3": self.CHANNEL_3_STRING}

        basic_glossary = {"AUX_PORT_MODE": self.AUX,
                          "SOLAR_CONNECTED_TO_BREAKERS": self.SCTB,
                          "EXPLICIT_CHANNEL_MAP": self.EXPLICIT_CM}
        to_update = {}
        for param, value in self.channelMap.items():
            if type(value) == list:
                to_str = ",".join([str(e) for e in value])
                self.channelMap.update({param: to_str})
        self.update_strings()
        for param in self.channelMap.keys():
            if param in glossary.keys():
                if self.channelMap[param] != glossary[param]:
                    check_ls = glossary[param].split(",")
                    while check_ls[2] == "NONE":
                        self.update_strings()
                        if "NONE" in self.channelMap[param]:
                            self.warning_message = QMessageBox()
                            self.warning_message.setText(
                                "Cannot set {} with NONE as type\nNo changes made.".format(param))
                            self.warning_message.show()
                            return
                        else:
                            continue

                    value = glossary[param]
                    to_update.update({param: value})
        for param in basic_glossary.keys():
            if param in basic_glossary.keys():
                if self.channelMap[param] != basic_glossary[param]:
                    self.__ui_logger("Updating: {} to {}".format(
                        param, basic_glossary[param]))
                    # print("UPDATE: {} to {}".format(param, basic_glossary[param]))
                    to_update.update({param: basic_glossary[param]})
        if len(to_update.keys()) > 0:
            sleep(2)
            self.error_message.setText(
                "Updated {} parameters successfully".format(len(to_update.keys())))
            self.error_message.show()
        else:
            sleep(2)
            self.error_message.setText("Detected {} changes.")
            self.error_message.show()
        for param, value in to_update.items():
            print(param, value, self.monitor.update_channelmap(param, value))
            sleep(1.5)
        self.load_house()

        # voltage = current_values[0]
        # ct = current_values[1]
        # type = current_values[2]

    def convert_dcm_to_list(self, channelmap):
        output = {}
        for param, values in channelmap.items():
            if values.__contains__(","):
                values = values.split(",")
            output.update({param: values})
        return output

    def __load_old_channel_map(self):
        channel_map = self.channelMap
        try:
            self.ConfigPage_ComboBox_MainCTPolarity.setCurrentText(
                channel_map["MAIN_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.channelMap.update({"MAIN_CT_POLARITY_BACKWARDS": "0,0"})

        try:
            self.ConfigPage_ComboBox_SolarCTPolarity.setCurrentText(
                channel_map["SOLAR_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.channelMap.update({"SOLAR_CT_POLARITY_BACKWARDS": "0,0"})
            self.ConfigPage_ComboBox_SolarCTPolarity.setCurrentText(
                channel_map["SOLAR_CT_POLARITY_BACKWARDS"])

        try:
            self.ConfigPage_ComboBox_SwapMains.setCurrentText(
                channel_map["LEFT_RIGHT_MAINS_SWAPPED"])
        except KeyError:
            self.channelMap.update({"LEFT_RIGHT_MAINS_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SwapMains.setCurrentText(
                channel_map["LEFT_RIGHT_MAINS_SWAPPED"])

        try:
            self.ConfigPage_ComboBox_SwapSolar.setCurrentText(
                channel_map["LEFT_RIGHT_SOLAR_SWAPPED"])
        except KeyError:
            self.channelMap.update({"LEFT_RIGHT_SOLAR_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SwapSolar.setCurrentText(
                channel_map["LEFT_RIGHT_SOLAR_SWAPPED"])
        try:
            # self.ConfigPage_ComboBox_UsingSolar.setCurrentText(
            #     channel_map["USING_SOLAR"])
            if channel_map["USING_SOLAR"] == 1:
                self.channelMap.update({"AUX_PORT_MODE": "solar"})
            else:
                # if len(channel_map["aux_port_mode"]) > 0:
                #     self.ConfigPage_ComboBox_UsingSolar.setCurrentText(
                #         channel_map["aux_port_mode"])
                # else:
                self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        except KeyError:
            self.__ui_logger("KEY_ERROR: USING_SOLAR")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        try:
            self.channelMap.update(
                {"AUX_PORT_MODE": channel_map["AUX_PORT_MODE"]})
        except KeyError:
            self.__ui_logger("KEY ERROR: AUX_PORT_MODE")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

        try:
            self.ConfigPage_ComboBox_SolarConnectedToBreakers.setCurrentText(
                channel_map["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.__ui_logger(
                "SOLAR_CONNECTED_TO_BREAKERS not found, setting to default (0).")
            self.ConfigPage_ComboBox_SolarConnectedToBreakers.setCurrentText(
                "0")

        try:
            self.ConfigPage_LineEdit_FrameRate.setText(
                channel_map["FRAME_RATE"])
        except KeyError:
            self.__ui_logger("KEY_ERROR: FRAME_RATE not set. Setting to 60...")
            self.ConfigPage_LineEdit_FrameRate.setText("60")

        try:
            self.ConfigPage_LineEdit_NumChannels.setText(
                channel_map["NUM_MAINS_CHANNELS"])
        except KeyError:
            self.__ui_logger(
                "KEY_ERROR: NUM_CHANNELS not set, setting to 2...")
            self.ConfigPage_LineEdit_NumChannels.setText("2")

        try:
            self.ConfigPage_ComboBox_SolarMainsSwapped.setCurrentText(
                channel_map["SOLAR_MAINS_SWAPPED"])
        except KeyError:
            self.channelMap.update({"SOLAR_MAINS_SWAPPED": "0"})
            self.ConfigPage_ComboBox_SolarMainsSwapped.setCurrentText(
                channel_map["SOLAR_MAINS_SWAPPED"])
        self.__ui_logger("Loaded current channel map")

    def __update_old_channelmap(self):
        # TODO
        glossary = {"LEFT_RIGHT_MAINS_SWAPPED": self.ConfigPage_ComboBox_SwapMains.currentText(),
                    "LEFT_RIGHT_SOLAR_SWAPPED": self.ConfigPage_ComboBox_SwapSolar.currentText(),
                    "MAIN_CT_POLARITY_BACKWARDS": self.ConfigPage_ComboBox_MainCTPolarity.currentText(),
                    "SOLAR_CT_POLARITY_BACKWARDS": self.ConfigPage_ComboBox_SolarCTPolarity.currentText(),
                    "USING_SOLAR": self.ConfigPage_ComboBox_UsingSolar.currentText(),
                    "SOLAR_CONNECTED_TO_BREAKERS": self.ConfigPage_ComboBox_SolarConnectedToBreakers.currentText(),
                    "FRAME_RATE": self.ConfigPage_LineEdit_FrameRate.text(),
                    "NUM_MAINS_CHANNELS": self.ConfigPage_LineEdit_NumChannels.text(),
                    "SOLAR_MAINS_SWAPPED": self.ConfigPage_ComboBox_SolarMainsSwapped.currentText(),
                    "AUX_PORT_MODE": self.channelMap["AUX_PORT_MODE"]}
        to_update = {}
        for parameter, value in glossary.items():
            try:
                if parameter not in self.channelMap.keys():
                    print(parameter)
            except KeyError as e:
                print("KEY_ERROR: {}".format(e))
        # for parameter in self.channelMap.keys():
        #     try:
        #         if parameter not in glossary.keys():
        #             print(parameter)
        #     except KeyError as e:
        #         pass
        # for parameter, channel_map_value in self.channelMap.items():
        #     try:
        #         if channel_map_value != glossary[parameter]:
        #             to_update.update({glossary: glossary[parameter]})
        #     except KeyError as e:
        #         print(e)
        # for current_parameter, current_value in self.channelMap.items():
        #     try:
        #         new_value = glossary[current_parameter]
        #         if current_value != new_value:
        #             self.__ui_logger("Updating {} from {} to {}".format(
        #                 current_parameter, current_value, new_value))
        #             to_update.update({current_parameter, new_value})
        #     except KeyError as e:
        #         print(e)
        pprint(to_update)

        # if count > 0:
        #     self.__ui_logger("{} changes detected...".format(count))
        #     # Update config
        #     self.__ui_logger(
        #         "Attempting to update config for h{}".format(self.monitor_id))
        #     update_conf = self.monitor_device.update_config()
        #     if update_conf == 200:
        #         self.__ui_logger(
        #             "Updated config successfully for h{}".format(self.monitor_id))
        #         # restart wattscoop
        #         self.__ui_logger(
        #             "Attempting to restart wattscoop on h{}.".format(self.monitor_id))
        #         restart_wattscoop = self.monitor_device.restart_service(
        #             "wattscoop")
        #         if restart_wattscoop == 200:
        #             self.__ui_logger("Wattscoop restarted successfully.")
        #             self.__ui_logger(
        #                 "Attempting to request hello on h{}.".format(self.monitor_id))
        #             # request hello
        #             self.__ui_logger(
        #                 "Sleeping for 5 seconds to allow wattscoop to restart...")
        #             sleep(5)
        #             hello = self.monitor_device.request_hello()
        #             if hello == 200:
        #                 self.__ui_logger(
        #                     "Hello restarted successfully for h{}".format(self.monitor_id))
        #                 self.error_message.setText(
        #                     "Successfully updated configuration.")
        #                 self.error_message.show()
        #             else:
        #                 self.__ui_logger(
        #                     "Request hello failed for h{}.".format(self.monitor_id))
        #                 self.error_message.setText(
        #                     "Restart hello for h{}.".format(self.monitor_id))
        #                 self.error_message.show()
        #         else:
        #             self.__ui_logger(
        #                 "Restart wattscoop failed for h{}".format(self.monitor_id))
        #             self.error_message.setText(
        #                 "Failed to restart wattscoop for h{}".format(self.monitor_id))
        #             self.error_message.show()
        #     else:
        #         self.__ui_logger(
        #             "Failed to update config for h{}".format(self.monitor_id))

    def load_old_channel_map(self):
        try:  # main polarity
            self.main_polarity.setCurrentText(
                self.channelMap["MAIN_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.main_polarity.setCurrentText("0,0")
            self.channelMap.update({"MAIN_CT_POLARITY_BACKWARDS": "0,0"})

        try:  # Solar polarity
            self.solar_polarity.setCurrentText(
                self.channelMap["SOLAR_CT_POLARITY_BACKWARDS"])
        except KeyError:
            self.solar_polarity.setCurrentText("0,0")
            self.channelMap.update({"SOLAR_CT_POLARITY_BACKWARDS": "0,0"})

        try:  # Swap mains
            self.swap_mains.setCurrentText(
                self.channelMap["LEFT_RIGHT_MAINS_SWAPPED"])
        except KeyError:
            self.swap_mains.setCurrentText("0")
            self.channelMap.update({"LEFT_RIGHT_MAINS_SWAPPED": "0"})

        try:  # Swap Solar
            self.swap_solar.setCurrentText(
                self.channelMap["LEFT_RIGHT_SOLAR_SWAPPED"])
        except KeyError:
            self.swap_solar.setCurrentText("0")
            self.channelMap.update({"LEFT_RIGHT_SOLAR_SWAPPED": "0"})

        try:  # sctb
            self.sctb.setCurrentText(
                self.channelMap["SOLAR_CONNECTED_TO_BREAKERS"])
        except KeyError:
            self.sctb.setCurrentText("0")
            self.channelMap.update({"SOLAR_CONNECTED_TO_BREAKERS": "0"})

        try:  # frame rate
            self.frame_rate.setText(self.channelMap["FRAME_RATE"])
        except KeyError:
            self.frame_rate.setText("60")
            self.channelMap.update({"FRAME_RATE": "60"})

        try:  # number of main channels
            self.num_main_channels.setText(
                self.channelMap["NUM_MAINS_CHANNELS"])
        except KeyError:
            self.num_main_channels.setText("2")
            self.channelMap.update({"NUM_MAINS_CHANNELS": "2"})

        try:  # Solar mains swapped
            self.solar_mains_swapped.setCurrentText(
                self.channelMap["SOLAR_MAINS_SWAPPED"])
        except KeyError:
            self.solar_mains_swapped.setCurrentText("0")
            self.channelMap.update({"SOLAR_MAINS_SWAPPED": "0"})

        try:  # aux_port_mode
            if "USING_SOLAR" in self.channelMap.keys():
                if self.channelMap["USING_SOLAR"] == "1":
                    self.aux_port_mode.setCurrentText("solar")
                if "AUX_PORT_MODE" in self.channelMap.keys():
                    self.aux_port_mode.setCurrentText(
                        self.channelMap["AUX_PORT_MODE"])
            else:
                self.aux_port_mode.setCurrentText(
                    self.channelMap["AUX_PORT_MODE"])
        except KeyError:
            self.aux_port_mode.setCurrentText("ignore")
            self.channelMap.update({"AUX_PORT_MODE": "ignore"})

    # def update_monitor_config(self, channel_map):
    #     pass

    def add_defaults_to_current_channelmap(self):
        DEFAULT = {"MAIN_CT_POLARITY_BACKWARDS": "0,0",
                   "SOLAR_CT_POLARITY_BACKWARDS": "0,0",
                   "LEFT_RIGHT_MAINS_SWAPPED": "0",
                   "LEFT_RIGHT_SOLAR_SWAPPED": "0",
                   "AUX_PORT_MODE": "ignore",
                   "SOLAR_MAINS_SWAPPED": "0",
                   "FRAME_RATE": "60",
                   "NUM_MAINS_CHANNELS": "2"}

    def update_old_channel_map(self):
        online_status = self.monitor.get_online_status()
        self.__ui_logger("Online Status: {}".format(online_status))
        if not online_status:
            # monitor offline
            self.error_message.setText(
                "h{} is offline, cannot update".format(self.monitor.monitor_id))
            self.error_message.show()
            return
        updated_channel_map = {
            "MAIN_CT_POLARITY_BACKWARDS": self.main_polarity.currentText(),
            "SOLAR_CT_POLARITY_BACKWARDS": self.solar_polarity.currentText(),
            "LEFT_RIGHT_MAINS_SWAPPED": self.swap_mains.currentText(),
            "LEFT_RIGHT_SOLAR_SWAPPED": self.swap_solar.currentText(),
            "AUX_PORT_MODE": self.aux_port_mode.currentText(),
            "FRAME_RATE": self.frame_rate.text(),
            "NUM_MAINS_CHANNELS": self.num_main_channels.text(),
            "SOLAR_CONNECTED_TO_BREAKERS": self.sctb.currentText(),
            "SOLAR_MAINS_SWAPPED": self.solar_mains_swapped.currentText()
        }
        updater = {}
        for parameter, original_value in self.channelMap.items():
            try:
                new_value = updated_channel_map[parameter]
                if new_value != original_value:
                    updater.update({parameter: new_value})
            except KeyError as e:
                self.__ui_logger("Key Error for {}, KEY: {}".format(
                    self.monitor.monitor_id, e))
                pass
        for parameter, value in updater.items():
            self.__ui_logger("Updating h{}: {}: {}".format(
                self.monitor.monitor_id, parameter, value))
            self.monitor.update_channelmap(parameter, value)
            self.channelMap.update({parameter: value})
            sleep(1)
        self.error_message.setText("Updated {} values for h{}".format(
            len(updater.keys()), self.monitor.monitor_id))
        self.error_message.show()

    def __ui_logger(self, message, INFO=True, WARN=False, ERROR=False):
        datestamp = datetime.utcnow()
        severity = "INFO"
        if INFO:
            logging.info("{} -- {}".format(datestamp, message))
        if WARN:
            severity = "WARN"
            logging.warning("{} -- {}".format(datestamp, message))
        elif ERROR:
            logging.error("{} -- {}".format(datestamp, message))
            severity = "ERROR"
        message = "{} : {} -- {}".format(severity, datestamp, message)
        self.LoggingPage_TextEdit_Console.append(message)


def main():
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
