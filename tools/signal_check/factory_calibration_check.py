import os
import argparse
import subprocess
def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("sn", type=str)
    args = parser.parse_args()
    return args

def get_factory_logs(serial):
    key_dir = os.path.expanduser("~/code/supporttools/certs/factoryCTkey/id_rsa")
    identify_dir_one = serial[:4]
    identify_dir_two = serial[4:7]
    ls_dir = "ls /mnt/sdd/sense-mfg/by-serial/{}/{}/{}/mfg-logs/".format(identify_dir_one, identify_dir_two, serial)
    find_mfg = str(subprocess.check_output(["ssh", "-i", key_dir, "alex@fenway", ls_dir], shell=False).decode('utf-8')).rstrip()
    cat_dir = "cat /mnt/sdd/sense-mfg/by-serial/{}/{}/{}/mfg-logs/{}/ctCalibration.conf".format(identify_dir_one,
                                                                                                identify_dir_two,
                                                                                                serial, find_mfg)
    command = "ssh -i {} alex@fenway {}".format(key_dir, cat_dir)
    os.system(command)


if __name__ == '__main__':
    args = parse_opts()
    serial = args.sn
    get_factory_logs(serial)

