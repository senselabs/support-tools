import argparse
nato_alphabet = {'a': 'alfa',
 'b': 'bravo',
 'c': 'charlie', 
 'd': 'delta', 
 'e': 'echo', 
 'f': 'foxtrot', 
 'g': 'golf', 
 'h': 'hotel',
'i': 'india',
'j': 'juliett',
'k': 'kilo',
'l': 'lima',
'm': 'mike',
'n': 'november',
'o': 'oscar',
'p': 'papa',
'q': 'quebec',
'r': 'romeo',
's': 'sierra',
't': 'tango',
'u': 'uniform',
'v': 'victor',
'w': 'whiskey',
'x': 'xray',
'y': 'yankee',
'z': 'zulu',
'0': 'zero',
'1': 'one',
'2': 'two',
'3': 'three',
'4': 'four',
'5': 'five',
'6': 'six',
'7': 'seven',
'8': 'eight',
'9': 'nine',
'.': 'dot',
' ': 'space-symbol',
'+': 'plus',
'-': 'dash',
'*': 'asterik-symbol',
'@': 'at-symbol'
}


def parse_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument('phrase', type=str, nargs='?')

    args = parser.parse_args()
    return args


def convert_phrase(phrase):
    nato_converted = [] 
    for letter in phrase.lower():
        if letter in nato_alphabet.keys():
            nato_converted.append(nato_alphabet[letter])
        else:
            nato_converted.append(letter)
    print('\n')
    print(' -> '.join(nato_converted))
    print('\nThe inputed phrase was: {}'.format(phrase))

def main():
    args = parse_opts()
    try:
        phrase = args.phrase
        convert_phrase(phrase)
    except AttributeError:
        print('\n')
        # If no input was given on command line
        phrase = input('Please enter your phrase to convert: ')
        if len(phrase) > 0:
            convert_phrase(phrase)
        elif len(phrase) == 0:
            while len(phrase) == 0:
                print('No input given...\n')
                phrase = input('Please input a phrase: ')
            if len(phrase) > 0:
                convert_phrase(phrase)

main()