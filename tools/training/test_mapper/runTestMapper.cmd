#!/bin/tcsh -f

if ( -e core ) then
	echo "remove existing core file"
exit
endif

unlimit coredumpsize

set profileStr = ""
set profile = `printenv S3_PROFILE`
if ( "$profile" != "" ) then
   set profileStr = "--profile $profile"
endif

set userID = `id -un`
source $HOME/code/tools/p4.0/AWS/setDefaultGoProfile.tcsh 

$HOME/code/tools/p3.0/QT/Mapper/Mapper ~/code/support-tools/tools/training/test_mapper/testModelsList.txt -w60 s3://combined-monitor-data \
    -uehd \
    -lgdir /data/house-models \
    -p4lg 

if ( -e core ) then
	cp testModelsList.txt testModelsList.crashed.txt
	echo "mapper crashed"
endif