#!/bin/bash

house=$1

curl -X POST --data-urlencode "payload={\"channel\": \"#mapperqueue\", \"username\": \"$2\", \"text\": \"Clustering house \", \"icon_emoji\": \":no_entry_sign:\"}" https://hooks.slack.com/services/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
ssh ubuntu@remapper.ds.sense.com -X /home/ubuntu/code/tools/p4.0/CM/runIdentityClusterStep.cmd $house all 60
curl -X POST --data-urlencode "payload={\"channel\": \"#mapperqueue\", \"username\": \"$2\", \"text\": \"Done\", \"icon_emoji\": \":white_check_mark:\"}" https://hooks.slack.com/services/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