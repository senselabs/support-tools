import argparse
from Sense.AwsUtils import Buckets
from pprint import pprint


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("--w60", dest="only_wattage", action="store_true", default=False)
    parser.add_argument("--s60", dest="only_solar", action="store_true", default=False)

    args = parser.parse_args()
    return args

def only_w60s(output):
    tmp = []
    for row in output:
        if "w60.gz" in row:
            tmp.append(row)
    return tmp

def only_s60s(output):
    tmp = []
    for row in output:
        if "s60.gz" in row:
            tmp.append(row)
    return tmp


def main():
    args = parse_opts()
    process = Buckets(args.house)
    try:
        output = process.get_monitor_files()
        if args.only_wattage:
            output = only_w60s(output)
        for line in output:
            if "PRE" not in line:
                print(line)
    except TypeError:
        print("Check house number.")


main()
