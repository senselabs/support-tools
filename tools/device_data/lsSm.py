import argparse
import subprocess
from Sense.AwsUtils import Buckets


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("mode", type=str, choices=[
                        "ls", "cat"], nargs="?", default="ls")
    parser.add_argument("model", type=str, nargs="?")
    parser.add_argument("-mv", "--model-version", type=int,
                        dest="model_version", default=None)

    args = parser.parse_args()
    return args


def get_file(bucket, model_version, model_name, mode):
    bucket = "{}/staging/models.V{}/{}".format(
        bucket, model_version, model_name)
    if mode == "cat":
        printSm = subprocess.Popen(
            ["s3", "cat", bucket], shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = printSm.communicate()
        out = stdout.decode("utf-8")
    elif mode == "ls":
        print(bucket)
        lsSm = subprocess.Popen(["aws", "s3", "ls", bucket],
                                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = lsSm.communicate()
        out = stdout.decode("utf-8")
    if out[0:2] != "An":
        print(out + "\n")
        print("Model Version: {}".format(model_version))
    else:
        print("Please ensure you provide a model file-name when using cat.")


def ls(bucket, model_version):
    bucket = bucket + "/staging/models.V{}/".format(model_version)
    cmd = subprocess.Popen(["aws", "s3", "ls", bucket], stdout=subprocess.PIPE)
    out = cmd.communicate()[0].decode("utf-8")
    if out[0:2] != "An":
        o = out.split(" ")
        i = [li for li in o if "sm" in li]
        for line in i:
            line = line.split("\n")[0]
            print(line)
        # print("Model Version: {}".format(model_version))
    else:
        print("Please ensure you provide a model file-name when using cat.")


def cat(bucket, model_version, model):
    bucket = bucket + "/staging/models.V{}/{}".format(model_version, model)
    print(bucket)
    cmd = subprocess.Popen(["s3", "cat", bucket], stdout=subprocess.PIPE)
    out = cmd.communicate()[0].decode("utf-8")
    print(out + "\nModel Version: {}".format(model_version))


def main():
    args = parse_opts()
    bucket = Buckets(args.house)
    house = bucket.monitor_id
    model = args.model
    model_version = args.model_version
    all_versions = bucket.get_model_version()
    all_versions.sort()
    # Check if model version is valid
    if model_version is None:
        model_version = all_versions[-1]

    else:
        if model_version in all_versions:
            model_version = model_version
        else:
            print("Model version: {} does not exist for h{}".format(
                model_version, house))
            exit()
    # Continue to get SM file
    if args.mode == "ls":
        ls(bucket.get_bucket_location(), model_version)
    elif args.mode == "cat":
        cat(bucket.get_bucket_location(), model_version, model)
    else:
        print("Please specify cat or ls.")


main()
