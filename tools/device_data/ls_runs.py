import argparse
from Sense.AwsUtils import Buckets


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)

    args = parser.parse_args()
    return args


def main():
    args = parse_opts()
    process = Buckets(args.house, "combined-monitor-data")
    try:
        print("Runs: {}".format(process.get_runs()))
    except TypeError:
        print("Check house number.")


main()