import argparse
import os
from subprocess import Popen, STDOUT, PIPE
from pprint import pprint

from Sense.AwsUtils import Buckets


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("-r", "--run", nargs="?", default=None, dest="run")
    args = parser.parse_args()
    return args


def get_run(bucket, run, monitor_id):
    runs = bucket.get_runs()
    if run is None:
        run = runs[-1]
        return run
    else:
        if run in runs:
            return run
        else:
            print("Run: {} not found for house: h{}".format(run, monitor_id))
            return None


def run_process(bucket, run):
    loc = bucket.get_bucket_location() + "/h{}.run{}.log.gz".format(bucket.monitor_id, run)
    check_logs = ["s3", "ls", bucket.get_bucket_location() + "/"]
    process = Popen(check_logs, stdout=PIPE)
    stdout, stderr = process.communicate(process)
    stdout = str(stdout).replace(" ", "").split(bucket.monitor_id)
    logs_only = [f for f in stdout if "log.gz" in f]
    print(f"OUT: {logs_only}")
    print(f"ERR: {stderr}")
    print(bucket.get_bucket_location())
    # cmd1 = ["s3", "cp", loc, "-"]
    # print(cmd1)
    # process1 = Popen(cmd1, stdout=PIPE)
    # # print(os.path.exists(f"/tmp/{loc}"))
    # cmd2 = ["zcat"]
    # process2 = Popen(cmd2, stdin=process1.stdout, stdout=PIPE)
    # cmd3 = ["head", "-n", "150"]
    # process3 = Popen(cmd3, stdin=process2.stdout, stdout=PIPE)
    # stdout = process3.communicate()[0].decode("utf-8")
    # print(stdout + "\n Run: {}".format(run))


def main():
    args = parse_opts()
    bucket = Buckets(args.house)
    run = get_run(bucket, args.run, args.house)
    if run is not None:
        run_process(bucket, run)


main()
