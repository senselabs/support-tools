import argparse
import os
import sys
from getpass import getpass
from time import sleep
from Sense.Api import Auth, Monitor

import requests


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument('house', type=str)
    parser.add_argument('merged_id', type=str)

    args = parser.parse_args()

    return args


def get_merged_devices(monitor, device_id):
    try:
        r = monitor.get_devices(device_id=device_id)
        print("Merged device IDs: {}".format(r["tags"]["MergedDevices"]))
    except KeyError:
        print("Device ID: {} does not exist for h{}".format(
            device_id, monitor.monitor_id))


# def getMergedDevices(house, device_id, auth):
#     url = "https://api.sense.com/api/v1/admin/monitors/" + \
#         house + "/devices/" + device_id
#     try:
#         response = requests.request("GET", url, headers=auth).json()
#         print("Merged Device Ids: {} ".format(
#             response['tags']['MergedDevices']))
#     except KeyError:
#         print('Device ID and house number combination incorrect. Try again.')


def main():
    args = parse_opts()
    monitor = Monitor(args.house, Auth().get_token())
    device_id = args.merged_id
    get_merged_devices(monitor, device_id)


main()
