import argparse
import subprocess
from Sense.Organize import OrganizeHouse


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("h", type=str)

    args = parser.parse_args()
    return args


# s3 cat s3://house-models/m000/t030/h30615/elec-vehicle/models.V33/code_version.yaml


def get_latest_ev_version(house, m_placement, t_placement):
    lsSm_cmd = "s3://house-models/{}/{}/h{}/elec-vehicle/".format(
        m_placement, t_placement, house)
    ls_out = subprocess.Popen(["aws", "s3", "ls", lsSm_cmd],
                              shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = ls_out.communicate()
    rows = stdout.decode("utf-8").split()
    model_versions = []
    for row in rows:
        if row != "PRE":
            model_num = str(row).rstrip("/").strip("models.V")
            model_versions.append(int(model_num))
    latest = max(model_versions)
    return latest


def get_file(house, m_placement, t_placement, ev_version):
    printSm_cmd = "s3://house-models/{}/{}/h{}/elec-vehicle/models.V{}/detector.log".format(
        m_placement, t_placement, house, ev_version)
    printSm = subprocess.Popen(["s3", "cat", printSm_cmd], shell=False,
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = printSm.communicate()
    out = stdout.decode("utf-8")

    print(out)
    print("\n")
    print("EV Detector Version: {}".format(ev_version))


def main():
    args = parse_opts()
    try:
        organizer = OrganizeHouse(args.h)
        latest_ev_version = get_latest_ev_version(
            organizer.unify, organizer.millions, organizer.thousands)
        get_file(organizer.unify, organizer.millions,
                 organizer.thousands, latest_ev_version)
    except ValueError:
        print("Possibly invalid house number. Please double check")


main()
