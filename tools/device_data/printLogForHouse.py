import argparse
import os
from subprocess import Popen, STDOUT, PIPE
from pprint import pprint

from Sense.AwsUtils import Buckets


def parse_opts():
    parser = argparse.ArgumentParser()
    parser.add_argument("house", type=str)
    parser.add_argument("-r", "--run", nargs="?", default=None, dest="run")
    args = parser.parse_args()
    return args


def get_run(bucket, run, monitor_id):
    runs = bucket.get_runs()
    if run is None:
        run = runs[-1]
        return run
    else:
        if int(run) in runs:
            return run
        else:
            print("Run: {} not found for house: h{}".format(run, monitor_id))
            return None


def run_process(bucket, run=None):
    # if not run:
    check_logs_exists = ["aws", "s3", "ls", bucket.get_bucket_location() + "/"]
    process = Popen(check_logs_exists, stdout=PIPE)
    isolate_logs = ["grep", "log.gz"]
    process = Popen(isolate_logs, stdin=process.stdout, stdout=PIPE).communicate()
    stdout = process[0].decode("UTF-8")
    li = stdout.replace("\n", "").split(".")
    valid_logs = [log for log in li if "run" in log]
    output = []
    for log in valid_logs:
        log = int(log.replace("run", ""))
        output.append(log)
    output.sort()
    run = output[-1]

    loc = bucket.get_bucket_location() + "/h{}.run{}.log.gz".format(bucket.monitor_id, run)
    cmd1 = ["aws", "s3", "cp", loc, "-"]
    process1 = Popen(cmd1, stdout=PIPE)
    cmd2 = ["zcat"]
    process2 = Popen(cmd2, stdin=process1.stdout, stdout=PIPE)
    cmd3 = ["head", "-n", "150"]
    process3 = Popen(cmd3, stdin=process2.stdout, stdout=PIPE)
    stdout = process3.communicate()[0].decode("utf-8")
    print(stdout + "\n Run: {}".format(run))


def main():
    args = parse_opts()
    bucket = Buckets(args.house)
    run = get_run(bucket, args.run, args.house)
    if run is not None:
        run_process(bucket, run)


main()
